<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Productos</title>
</head>

<script src="js/jquery.min.js"></script>
<script src="js/segundas.js"></script>


<body>  

{!!Form::open(array('url'=>'insertar','method'=>'POST','autocomplete'=>'off'))!!}

    {!!Form::label('producto') !!}<br>
    {!!form::text('producto',null)!!}
    <br>
    {!!Form::label('precio') !!}<br>
    {!!form::text('precio',null,['id'=>'idpreciotxt'])!!}
    <br>
    {!!Form::label('cantidad') !!}<br>
    {!!form::text('cantidad',null,['id'=>'idcantidadtxt'])!!}
    <br>
    {!!Form::label('descuento') !!}<br>
    {!!form::text('descuento',null,['id'=>'iddescuentotxt'])!!}
    <br>
    {!!Form::label('precio_final') !!}<br>
    {!!form::text('precio_final',null,['id'=>'idpreciofinaltxt'])!!}
    <br><br>
    {!!form::submit('Registrar',['name'=>'grabar','id'=>'grabar','content'=>'<span>Registrar</span>'])!!}
    {!!Form::close()!!}
    
</body>
</html>