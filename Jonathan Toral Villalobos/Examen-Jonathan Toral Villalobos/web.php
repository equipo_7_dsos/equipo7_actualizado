<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); 
});

//examen
Route::get('formulario','ProductoController@formu');
Route::post('insertar','ProductoController@insertarproducto');

Route::get('eliminar/{id}','ProductoController@eliminar');

Route::get('bandera/{id}','ProductoController@bandera');


Route::get('actualizar/{id}','ProductoController@editar_datos');
Route::put('actualizar_producto/{id}','ProductoController@actualizar');

