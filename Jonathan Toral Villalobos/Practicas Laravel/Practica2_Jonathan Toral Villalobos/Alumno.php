<?php

namespace App\Models\DSOS;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table='alumno';

    protected $primaryKey='idalumno';

    public $timestamps=false;


    protected $fillable =[
        'idalumno',
        'nombre',
        'apellido_p',
        'apellido_m',
        'edad',
        'direccion',
        'telefono'
    ];
}
