<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class prac3Model extends Model
{
	protected $table= 'tienda';
	protected $primarykey='id';
	public $timestamps=false;
     
	protected $fillable=[
     'id',
     'razon_social',
     'rfc',
     'nombre',
     'direccion',
     'tipo_empresa',
     'telefono',
     'fecha_ingreso'
	];
}