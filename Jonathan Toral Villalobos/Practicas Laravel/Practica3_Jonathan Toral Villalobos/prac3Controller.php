<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\prac3Model; 
class prac3Controller extends Controller
{
    public function insertar(Request $datos){
    	$razon_social= $datos->input('razon_social');
    	$rfc= $datos->input('rfc');
    	$nombre_d= $datos->input('nombre');
    	$tipo_empresa=$datos->input('tipo_empresa');
    	$direccion=$datos->input('direccion');
    	$telefono=$datos->input('telefono');
        $fecha_ingreso=$datos->input('fecha_ingreso');
    	  
    	prac3Model::create(['razon_social'=>$razon_social,'rfc'=>$rfc,'nombre'=>$nombre_d,'tipo_empresa'=>$tipo_empresa,'direccion'=>$direccion,'telefono'=>$telefono,'fecha_ingreso'=>$fecha_ingreso]);
        return redirect()->to('formulario');

    }

     public function ver_formulario(){
      return view('prac3Vista');
     }

}
