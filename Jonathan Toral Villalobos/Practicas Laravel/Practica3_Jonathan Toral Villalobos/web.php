<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//practica 3
Route::get('formulario','prac3Controller@ver_formulario');
Route::post('insertar','prac3Controller@insertar');

Route::get('/', function () {
    return view('welcome');
});