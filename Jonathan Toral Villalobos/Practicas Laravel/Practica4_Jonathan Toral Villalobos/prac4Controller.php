<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\prac4Model; 
class prac4Controller extends Controller
{
    public function insertar(Request $datos){
    	$rfc= $datos->input('rfc');
    	$curp= $datos->input('curp');
    	$numero_c= $datos->input('numero_c');
    	$materia1=$datos->input('materia1');
    	$materia2=$datos->input('materia2');
    	$materia3=$datos->input('materia3');
        $calf_m1=$datos->input('calf_m1');
        $calf_m2=$datos->input('calf_m2');
        $calf_m3=$datos->input('calf_m3');
        $promedio=$datos->input('promedio');
        $fecha_i=$datos->input('fecha_i');
    	  
    	prac4Model::create(['rfc'=>$rfc,'curp'=>$curp,'numero_c'=>$numero_c,'materia1'=>$materia1,'materia2'=>$materia2,'materia3'=>$materia3,'calf_m1'=>$calf_m1,'calf_m2'=>$calf_m2,'calf_m3'=>$calf_m3,'promedio'=>$promedio,'fecha_i'=>$fecha_i]);
        return redirect()->to('formpractica4');

    }

     public function ver_formulario(){
      return view('prac4/prac4Create');
     }

     public function editar_datos($id){
      $res=prac4Model::where('id',$id)->take(1)->first();//muestra solamente un registro
      return view('prac4/prac4Update')->with('res',$res); 
     }

     public function ver_datos(){
         $res=prac4Model::
         select('id','rfc','curp','numero_c','materia1','materia2','materia3','calf_m1','calf_m2','calf_m3','promedio','fecha_i')->take(1)->first();
         return view('prac4/prac4Update')->with('res',$res); 
    }

    public function update(Request $data,$id)
    {
      $editar = prac4Model::find($id);
      $editar->id = $data->id;
      $editar->rfc = $data->rfc;
      $editar->curp = $data->curp;
      $editar->numero_c = $data->numero_c;
      $editar->materia1 = $data->materia1;
      $editar->materia2 = $data->materia2;
      $editar->materia3 = $data->materia3;
      $editar->calf_m1 = $data->calf_m1;
      $editar->calf_m2 = $data->calf_m2;
      $editar->calf_m3 = $data->calf_m3;
      $editar->promedio = $data->promedio;
      $editar->fecha_i = $data->fecha_i;
      $editar->save();
      return redirect()->to('vistpractica4');

    }

}
