<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//practica 3
Route::get('formulario','prac3Controller@ver_formulario');
Route::post('insertar','prac3Controller@insertar');

//practica 4
Route::get('formpractica4','prac4Controller@ver_formulario');
Route::post('insertarprac4','prac4Controller@insertar');
Route::get('actualizarprac4/{id}','prac4Controller@editar_datos');
Route::put('updateprac4/{id}','prac4Controller@update');
Route::get('vistpractica4','prac4Controller@ver_datos');

Route::get('/', function () {
    return view('welcome');
});