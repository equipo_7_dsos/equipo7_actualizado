<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class prac4Model extends Model
{
	protected $table= 'estudiante';
	protected $primarykey='id';
	public $timestamps=false;
     
	protected $fillable=[
     'id',
     'rfc',
     'curp',
     'numero_c',
     'materia1',
     'materia2',
     'materia3',
     'claf_m1',
     'claf_m3',
     'claf_m4',
     'promedio',
     'fecha_i'
	];
}