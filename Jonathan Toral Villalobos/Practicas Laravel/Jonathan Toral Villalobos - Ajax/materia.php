<?php

namespace App\Models\AJAX;

use Illuminate\Database\Eloquent\Model;

class materia extends Model
{
    protected  $table = 'materias';

    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
      'id','nombre_materia','semestre'
    ];
}
