<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/
//AJAX
Route::get('lista_alumnos/{genero}','AJAx\AjaxController@listado_alumnos');

Route::get('ajax','AJAx\AjaxController@formu');



//practica ajax

Route::get('lista_materias/{genero}','AJAx\prac_ajax_controller@listado_materias');

Route::get('formulario_ajax','AJAx\prac_ajax_controller@formulario_ajax');

Route::post('insertar_ajax','AJAx\prac_ajax_controller@insertar');


Route::get('/', function () {
	return view('welcome');
});




