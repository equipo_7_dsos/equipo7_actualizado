<?php

namespace App\Models\AJAX;

use Illuminate\Database\Eloquent\Model;

class semestre extends Model
{
    protected  $table = 'semestre';

    protected $primarykey = 'id';
    public $timestamps = false;
    
    protected $fillable = [
      'id','semestre','materia'
    ];
}
