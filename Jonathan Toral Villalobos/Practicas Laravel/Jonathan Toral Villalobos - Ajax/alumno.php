<?php

namespace App\Models\AJAX;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
    protected  $table = 'alumno_a';

    protected $primarykey = 'id';
    public $timestamps = false;

    protected $fillable = [
      'id','nombre_completo','num_control','materia','semestre'
    ];
}
