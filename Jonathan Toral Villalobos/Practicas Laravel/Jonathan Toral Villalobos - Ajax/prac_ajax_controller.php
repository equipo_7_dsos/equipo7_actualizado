<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\materia;
use App\Models\AJAX\alumno;
use App\Models\AJAX\semestre;

class prac_ajax_controller extends Controller
{
    public function listado_materias($materia)
    {
        $al = semestre::select('id','semestre','materia')
        ->where('materia',$materia)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = materia::pluck('semestre','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }

    public function formulario_ajax()
    {
        $enviar = materia::pluck('nombre_materia','id');
        return view('AJAX/prac_ajax')->with('sex',$enviar);
    }
    public function insertar(Request $datos){
        $id= $datos->input('id');
        $nombre= $datos->input('nombre');
        $num_control= $datos->input('num_control');
        $materia=$datos->input('materia');
        $semestre=$datos->input('idsemestre');
        
        alumno::create(['id'=>$id,'nombre_completo'=>$nombre,'num_control'=>$num_control,'materia'=>$materia,'semestre'=>$semestre]);
        return redirect()->to('formulario_ajax');

    }
}
?>