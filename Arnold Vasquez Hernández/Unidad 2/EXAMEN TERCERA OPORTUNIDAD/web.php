<?php

/*

*/

Route::get('/', function () {
    return view('welcome');
});

//examen
Route::get('formulario','ProductoController@formu');
Route::post('insertar','ProductoController@insertarproducto');

Route::get('eliminar/{id}','ProductoController@eliminar');

Route::get('bandera/{id}','ProductoController@bandera');
Route::get('actualizar/{id}','ProductoController@editar_datos');
Route::put('actualizar_producto/{id}','ProductoController@actualizar');
