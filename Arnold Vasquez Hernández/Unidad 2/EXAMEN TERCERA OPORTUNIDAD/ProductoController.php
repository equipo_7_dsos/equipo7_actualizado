<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;


class ProductoController extends Controller
{
    public function formu()
    {
        return view('/vista');
    }

    public function insertarproducto(Request $datos){

        $producto= $datos->input('producto');
        $precio=$datos->input('precio');
        $cantidad=$datos->input('cantidad');
        $descuento=$datos->input('descuento');
        $precio_final=$datos->input('precio_final');

        Producto::create(['producto'=>$producto,'precio'=>$precio,'cantidad'=>$cantidad,'descuento'=>$descuento,'precio_final'=>$precio_final]);
        return redirect()->to('formulario');

    }

    public function eliminar($id){
        $dato=Producto::find($id);
        $dato->delete();
        return redirect()->to('formulario');
    }

    public function editar_datos($id){
        $uno=Producto::where('id',$id)->take(1)->first();//muestra solamente un registro
        return view('actualizar')->with('uno',$uno);
     }

    public function actualizar(Request $data,$id)
    {
      $editar = Producto::find($id);
      $editar->producto=$data->producto;
      $editar->precio = $data->precio;
      $editar->cantidad=$data->cantidad;
      $editar->descuento=$data->descuento;

      $editar->save();
      return redirect()->to('formulario');
    }

    public function bandera(Request $data,$id)//se puede quitar el request
    {
      $editar = Producto::find($id);
      $editar->estado='0';
      $editar->save();
      return redirect()->to('formulario');
    }

}
