<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\practica3_model; 
class practica3_controlador extends Controller
{
	
    
    public function insertar(Request $datos){
    	
        
    	$razon_social= $datos->input('razon_social');
    	$rfc= $datos->input('rfc');
    	$nombre_d= $datos->input('nombre_d');
    	$tipo_empresa=$datos->input('tipo_empresa');
    	$direccion=$datos->input('direccion');
    	$telefono=$datos->input('telefono');
        $fecha_ingreso=$datos->input('fecha_ingreso');
    	
          

    	practica3_model::create(['razon_social'=>$razon_social,'rfc'=>$rfc,'nombre_d'=>$nombre_d,'tipo_empresa'=>$tipo_empresa,'direccion'=>$direccion,'telefono'=>$telefono,'fecha_ingreso'=>$fecha_ingreso]);
        return redirect()->to('formulario');//para redireccionar a otra pagina

    }

     public function ver_formulario(){
      return view('vista_practica3');
     }

}
