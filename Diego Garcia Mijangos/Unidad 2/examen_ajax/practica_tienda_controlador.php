<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\empresa;
use App\Models\AJAX\producto;


class practica_tienda_controlador extends Controller
{
    public function lista_productos($materia)
    {
        $al = producto::select('id','nombre','tipo','provedor','precio_unitario','precio_venta')
        ->where('id',$materia)
        ->get();
          return $al;
    }


    public function formulario_empresa()
    {
        //$enviar = materia::pluck('precio_unitario','id');
        return view('AJAX/vista_insertar_empresa');
    }

     public function formulario_producto()
    {
       
        return view('AJAX/vista_insertar_producto');
    }
    public function vista_editar_producto()
    {
        $enviar = producto::pluck('nombre','id');
        return view('AJAX/vista_editar_producto')->with('prod',$enviar);
    }

   public function  editar_producto()
    {
        $enviar = producto::pluck('nombre','id');
        return view('AJAX/vista_editar_producto')->with('sex',$enviar);
    }

    public function insertarempresa(Request $datos){
        
        $rfc= $datos->input('rfc');
        $razon_social= $datos->input('razon_social');
        $direccion_fiscal= $datos->input('direccion_fiscal');
        $apoderado_legal=$datos->input('apoderado_legal');
        $telefono=$datos->input('telefono');
       
        
        empresa::create(['rfc'=>$rfc,'razon_social'=>$razon_social,'direccion_fiscal'=>$direccion_fiscal,'apoderado_legal'=>$apoderado_legal,'telefono'=>$telefono]);
        return redirect()->to('formulario_empresa');//para redireccionar a otra pagina

    }

    public function insertarproducto(Request $datos){
       
        $nombre= $datos->input('nombre');
        $tipo= $datos->input('tipo');
        $provedor= $datos->input('provedor');
        $precio_unitario=$datos->input('precio_unitario');
        $precio_venta=$datos->input('precio_venta');
       
        
        producto::create(['nombre'=>$nombre,'tipo'=>$tipo,'provedor'=>$provedor,'precio_unitario'=>$precio_unitario,'precio_venta'=>$precio_venta]);
        return redirect()->to('formulario_producto');//para redireccionar a otra pagina

    }

   
     public function editar_datos($id){
        $uno =empresa::where('id',$id)->take(1)->first();//muestra solamente un registro
        //$dos=practica3_model::find($id); hace lo mismo que $uno
      return view('AJAX/empresa_editar_datos')->with('uno',$uno); 
     }

      public function actualizar_datos(Request $data,$id)
    {
      $editar = empresa::find($id);
      $editar->rfc = $data->rfc;
      $editar->razon_social = $data->razon_social;
      $editar->direccion_fiscal = $data->apoderado_legal;
      $editar->telefono = $data->telefono;

      $editar->save();

      return redirect()->to('formulario_empresa');
    }

    public function eliminar_datos_bandera(Request $data,$id)//se puede quitar el request
    {
      $editar = empresa::find($id);
       
      $editar->activo='0';
      $editar->save();

      return redirect()->to('formulario_empresa');
    }
    

}
?>