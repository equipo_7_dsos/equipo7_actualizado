<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class practica4_model extends Model
{

	protected $table= 'alumnocalif';
	protected $primarykey='id';
	public $timestamps=false;
	protected $fillable=[
     'id','rfc','curp','numControl','materia1','materia2','materia3','cal1','cal2','cal3','promedio','fecha_alta'
	];

}