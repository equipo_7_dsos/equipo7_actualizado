<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\practica4_model; 
class practica4_controlador extends Controller
{
	
    
    public function insertar(Request $datos){
    	
   
    	$rfc= $datos->input('rfc');
    	$curp= $datos->input('curp');
    	$numControl=$datos->input('numControl');
    	$materia1=$datos->input('materia1');
      $materia2=$datos->input('materia2');
      $materia3=$datos->input('materia3');
      $cal1=$datos->input('cal1');
    	$cal2=$datos->input('cal2');
      $cal3=$datos->input('cal3');
      $promedio=$datos->input('promedio');
      $fecha_alta=$datos->input('fecha_alta');
    	
          
    	practica4_model::create(['rfc'=>$rfc,'curp'=>$curp,'numControl'=>$numControl,'materia1'=>$materia1,'materia2'=>$materia2,'materia3'=>$materia3,'cal1'=>$cal1,'cal2'=>$cal2,'cal3'=>$cal3,'promedio'=>$promedio,'fecha_alta'=>$fecha_alta]);
        return redirect()->to('formulario_p4');//para redireccionar a otra pagina

    }

     public function ver_formulario(){
      return view('vista_practica4');
     }

     
    //actualizar datos 
     public function editar_datos($id){
        $uno =practica4_model::where('id',$id)->take(1)->first();//muestra solamente un registro
        //$dos=practica3_model::find($id); hace lo mismo que $uno
      return view('vista_actualizar_practica4')->with('uno',$uno); 
     }

   
   public function ver_datos(){
         $uno=practica4_model::
         select('id','rfc','curp','numControl','materia1','materia2','materia3','cal1','cal2','cal3','promedio','fecha_alta')->take(1)->first();
         return view('vista_actualizar_practica4')->with('uno',$uno); 
    }


      public function actualizar_datos(Request $data,$id)
    {
      $editar = practica4_model::find($id);
       

      $editar->id = $data->id;
      $editar->rfc = $data->rfc;
      $editar->curp = $data->curp;
      $editar->numControl = $data->numControl;
      $editar->materia1 = $data->materia1;
      $editar->materia2 = $data->materia2;
      $editar->materia3 = $data->materia3;
      $editar->cal1 = $data->cal1;
      $editar->cal2 = $data->cal2;
      $editar->cal3 = $data->cal3;
      $editar->promedio = $data->promedio;
      $editar->fecha_alta = $data->fecha_alta;

      $editar->save();

      return redirect()->to('visualizar_p4');



    }

}
