<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 Route::get('/practica','dsos_controler\new_controlador@index');
 Route::get('/verdatos','dsos_controler\new_controlador@ver_datos');
Route::get('verdatosblade','dsos_controler\new_controlador@ver_datos_blade');
Route::get('practica2','practica2_controlador@ver_datos');
//insertar datos
Route::get('formulario','practica2_controlador@ver_formulario');
Route::post('insertar','practica2_controlador@insertar');

//practica 3 insertar datos
Route::get('formulario','practica3_controlador@ver_formulario');
Route::post('insertar','practica3_controlador@insertar');
//actualizar datos
Route::get('visualizar','practica3_controlador@ver_datos');
Route::get('actualizar/{id}','practica3_controlador@editar_datos');
Route::put('actualizar_datos/{id}','practica3_controlador@actualizar_datos');

//practica 4
Route::get('formulario_p4','practica4_controlador@ver_formulario');
Route::post('insertar_p4','practica4_controlador@insertar');
Route::get('actualizar_p4/{id}','practica4_controlador@editar_datos');
Route::put('actualizar_datos_p4/{id}','practica4_controlador@actualizar_datos');
Route::get('visualizar_p4','practica4_controlador@ver_datos');
Route::get('/', function () {
    return view('welcome');
});