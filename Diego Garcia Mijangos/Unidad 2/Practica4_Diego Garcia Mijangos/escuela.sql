-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2019 a las 20:16:03
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `escuela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `id` int(2) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidop` varchar(50) NOT NULL,
  `apellidom` varchar(50) NOT NULL,
  `edad` int(5) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `nombre`, `apellidop`, `apellidom`, `edad`, `direccion`, `telefono`) VALUES
(1, 'diego', 'garcia', 'mijangos', 22, 'dirección calle', 969499324),
(2, 'juan', 'peez', 'gonzales', 32, 'direcion de juan', 88338882),
(3, 'german', 'perez', 'perez', 42, 'direcion de german', 95338882),
(4, 'oscar', 'martinez', 'martinez', 26, 'direcion de oscar', 95313882),
(5, 'jose', 'garcia', 'gil', 33, 'direcion de jose', 48338882),
(6, 'pedro', 'santos', 'gonzales', 32, 'direcion de ´pedro', 98658882),
(7, 'ana', 'sanchez', 'gonzales', 24, 'direcion de ana', 98332482),
(8, 'maria', 'sarin', 'jimenez', 32, 'direcion de maria', 98432181),
(9, 'mario', 'santos', 'gonzo', 35, 'direcion de mario', 88638483),
(10, 'miguel', 'peres', 'gonzales', 32, 'direcion de miguel', 88338882),
(43, 'datos', 'datos', 'datos', 54, 'dire v', 99999999),
(56, 'nueo', 'oeoeo', 'ofoofof', 43, 'oeeooeo', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnocalif`
--

CREATE TABLE `alumnocalif` (
  `id` int(3) NOT NULL,
  `rfc` varchar(30) NOT NULL,
  `curp` varchar(50) NOT NULL,
  `numControl` int(15) NOT NULL,
  `materia1` varchar(50) NOT NULL,
  `materia2` varchar(50) NOT NULL,
  `materia3` varchar(50) NOT NULL,
  `cal1` int(2) NOT NULL,
  `cal2` int(2) NOT NULL,
  `cal3` int(2) NOT NULL,
  `promedio` int(3) NOT NULL,
  `fecha_alta` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumnocalif`
--

INSERT INTO `alumnocalif` (`id`, `rfc`, `curp`, `numControl`, `materia1`, `materia2`, `materia3`, `cal1`, `cal2`, `cal3`, `promedio`, `fecha_alta`) VALUES
(7, 'RIVTD944924', 'POCRF939530CNICC', 15160010, 'Programacion', 'Estadistica', 'Calculo', 10, 10, 10, 10, '2019-02-20'),
(8, 'APAP93920DKF', 'PAIFMI4030302MDMD', 14013010, 'Estadistica', 'Programacion', 'Calculo', 9, 9, 9, 9, '2019-02-20'),
(9, 'PERRE9301934NCIOR', 'O093ONCNON', 139103021, 'Programacion', 'Estadistica', 'Calculo', 10, 9, 8, 9, '2019-02-21'),
(10, 'GAMA8456O3KJN', 'GAMA9493939IRNFIO', 945682451, 'Calculo', 'Estadistica', 'Programacion', 8, 8, 8, 8, '2019-02-21'),
(11, 'IOAOA9452959NCJFJ', 'NJUCONE939391NCJ', 111211313, 'Programacion', 'Estadistica', 'Estadistica', 8, 9, 10, 9, '2019-02-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tienda`
--

CREATE TABLE `tienda` (
  `id` int(10) NOT NULL,
  `razon_social` varchar(70) NOT NULL,
  `rfc` varchar(50) NOT NULL,
  `nombre_d` varchar(80) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `tipo_empresa` varchar(100) NOT NULL,
  `telefono` int(12) NOT NULL,
  `fecha_ingreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tienda`
--

INSERT INTO `tienda` (`id`, `razon_social`, `rfc`, `nombre_d`, `direccion`, `tipo_empresa`, `telefono`, `fecha_ingreso`) VALUES
(1, '12345', '828282', 'diego', 'direcico', 'empresa', 9999999, '2019-02-15'),
(2, 'nuevajkncid', 'incindci', 'nodnwed', 'iecineic', 'nicedine', 8383838, '2019-02-16'),
(3, 'sdiocno', 'onp', 'onpm', 'pnop', 'pn', 99999, '2019-02-16'),
(4, 'kspni', 'ompo', 'okpo', 'okppo', 'okpop', 939393939, '2019-02-15'),
(5, '123456', '828282', 'diego', 'direcico', 'empresa', 9999999, '2019-02-15');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `alumnocalif`
--
ALTER TABLE `alumnocalif`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tienda`
--
ALTER TABLE `tienda`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnocalif`
--
ALTER TABLE `alumnocalif`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tienda`
--
ALTER TABLE `tienda`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
