-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-03-2019 a las 05:59:48
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `escuela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno_a`
--

CREATE TABLE `alumno_a` (
  `id` int(11) NOT NULL,
  `nombre_completo` varchar(60) NOT NULL,
  `num_control` varchar(30) NOT NULL,
  `curp` varchar(60) NOT NULL,
  `sexo` varchar(40) NOT NULL,
  `materia` varchar(30) NOT NULL,
  `semestre` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno_a`
--

INSERT INTO `alumno_a` (`id`, `nombre_completo`, `num_control`, `curp`, `sexo`, `materia`, `semestre`) VALUES
(1, 'DIEGO GARCIA', '15161301', 'FVDVFDVFVFDV', 'MASCULINO', '1', '4'),
(2, 'DIEGO GARCIA MIJANGOS', '15161301', 'ONCVSDNOVJ', 'MASCULINO', '1', '2');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno_a`
--
ALTER TABLE `alumno_a`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
