-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-02-2019 a las 22:57:45
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `escuela`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `id` int(2) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidop` varchar(50) NOT NULL,
  `apellidom` varchar(50) NOT NULL,
  `edad` int(5) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id`, `nombre`, `apellidop`, `apellidom`, `edad`, `direccion`, `telefono`) VALUES
(1, 'diego', 'garcia', 'mijangos', 22, 'dirección calle', 969499324),
(2, 'juan', 'peez', 'gonzales', 32, 'direcion de juan', 88338882),
(3, 'german', 'perez', 'perez', 42, 'direcion de german', 95338882),
(4, 'oscar', 'martinez', 'martinez', 26, 'direcion de oscar', 95313882),
(5, 'jose', 'garcia', 'gil', 33, 'direcion de jose', 48338882),
(6, 'pedro', 'santos', 'gonzales', 32, 'direcion de ´pedro', 98658882),
(7, 'ana', 'sanchez', 'gonzales', 24, 'direcion de ana', 98332482),
(8, 'maria', 'sarin', 'jimenez', 32, 'direcion de maria', 98432181),
(9, 'mario', 'santos', 'gonzo', 35, 'direcion de mario', 88638483),
(10, 'miguel', 'peres', 'gonzales', 32, 'direcion de miguel', 88338882);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
