<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class practica2_model extends Model
{

	protected $table= 'alumno';
	protected $primarykey='id';
	public $timestamps=false;
	protected $fillable=[
     'id','nombre','apellidop','apellidom','edad','direccion','telefono'
	];

}