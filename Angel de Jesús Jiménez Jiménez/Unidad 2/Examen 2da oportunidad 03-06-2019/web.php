<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('ws/{$id}','Auth\LoginController@obtieneapi');
Route::get('insertar_ws','Auth\LoginController@apiRegistro');
Route::get('actualizar_ws','Auth\LoginController@apiActualiza');


Route::post('insertar_e','ControladorApi@apiRegistra');

 Route::get('/practica','dsos_controler\new_controlador@index');
 Route::get('/verdatos','dsos_controler\new_controlador@ver_datos');
Route::get('verdatosblade','dsos_controler\new_controlador@ver_datos_blade');
Route::get('practica2','practica2_controlador@ver_datos');
//insertar datos
Route::get('formulario','practica2_controlador@ver_formulario');
Route::post('insertar','practica2_controlador@insertar');

//practica 3 insertar datos
Route::get('formulario','practica3_controlador@ver_formulario');
Route::post('insertar','practica3_controlador@insertar');
//actualizar datos
Route::get('visualizar','practica3_controlador@ver_datos');
Route::get('actualizar/{id}','practica3_controlador@editar_datos');
Route::put('actualizar_datos/{id}','practica3_controlador@actualizar_datos');

//practica 4
Route::get('formulario_p4','practica4_controlador@ver_formulario');
Route::post('insertar_p4','practica4_controlador@insertar');
Route::get('actualizar_p4/{id}','practica4_controlador@editar_datos');
Route::put('actualizar_datos_p4/{id}','practica4_controlador@actualizar_datos');
Route::get('visualizar_p4','practica4_controlador@ver_datos');



Route::get('insertar_practica4','Practica4Controlador@ver_formulario');
Route::post('insertar','Practica4Controlador@insertar');
Route::get('ver_datos','Practica4Controlador@ver_datos');
Route::get('edit_practica4/{id}','Practica4Controlador@edit_datos');
Route::get('eliminar_practica4/{id}','Practica4Controlador@edit_eliminar');
Route::put('eliminar_datos/{id}','Practica4Controlador@eliminar_datos');

//eliminar por bandera
//Route::get('actualizar_p4/{id}','practica4_controlador@');
Route::get('eliminarporbandera_datos/{id}','eliminar_bandera_controlador@editar_datos');
Route::put('eliminar_datos/{id}','eliminar_bandera_controlador@eliminar_datos_bandera');
 //tabla
Route::get('tabla','tabla_controlador@ver_tabla');
Route::get('actualizar_tabla/{id}','tabla_controlador@editar_datos');
Route::put('actualizar_datos_tabla/{id}','tabla_controlador@actualizar_datos');
Route::get('eliminar_bandera/{id}','tabla_controlador@eliminar_datos_bandera');
Route::get('eliminar_tabla/{id}','tabla_controlador@eliminar_datos');

//ferreteria
Route::get('formulario_ferreteria','ferreteria_controlador@ver_formulario');
Route::post('insertar_ferreteria','ferreteria_controlador@insertar');
Route::get('tabla_ferreteria','ferreteria_controlador@ver_tabla');
Route::get('actualizar_ferreteria/{id}','ferreteria_controlador@editar_datos');
Route::put('actualizar_datos_ferreteria/{id}','ferreteria_controlador@actualizar_datos');
Route::get('eliminar_bandera_ferreteria/{id}','ferreteria_controlador@eliminar_datos_bandera');
Route::get('eliminar_ferreteria/{id}','ferreteria_controlador@eliminar_datos');

//AJAX
Route::get('lista_alumnos/{genero}','AJAx\AjaxController@listado_alumnos');
Route::get('ajax','AJAx\AjaxController@formu');

//practica ajax

Route::get('lista_materias/{genero}','AJAx\practica_ajax_controlador@listado_materias');
Route::get('formulario_ajax','AJAx\practica_ajax_controlador@formulario_ajax');
Route::post('insertar_ajax','AJAx\practica_ajax_controlador@insertar');

//practica tienda de abarrotes
Route::get('formulario_empresa','AJAx\practica_tienda_controlador@formulario_empresa');
Route::post('insertar_empresa','AJAx\practica_tienda_controlador@insertarempresa');
Route::get('formulario_producto','AJAx\practica_tienda_controlador@formulario_producto');
Route::post('insertar_producto','AJAx\practica_tienda_controlador@insertarproducto');
Route::get('lista_productos/{producto}','AJAx\practica_tienda_controlador@lista_productos');
//Route::get('vista_editar_producto','AJAx\practica_tienda_controlador@editar_producto');
Route::get('vista_editar_producto','AJAx\practica_tienda_controlador@vista_editar_producto');

//examen
Route::get('actualizar_empresa/{id}','AJAx\practica_tienda_controlador@editar_datos');
Route::put('actualizar_datos_empresa/{id}','AJAx\practica_tienda_controlador@actualizar_datos');
Route::get('eliminar_bandera_empresa/{id}','AJAx\practica_tienda_controlador@eliminar_datos_bandera');









// EXAMEN 2DA OPORTUNIDAD
Route::get('vista_ventas','AJAx\practica_tienda_controlador@vista_editar_producto');


// INSERTAR


Route::get('formularioProducto','AJAx\practica_tienda_controlador@formularioProducto');

Route::POST('insertarProducto','AJAx\practica_tienda_controlador@insertarNP');



// ELIMINAR


Route::get('eliminarProducto/{id}','AJAx\practica_tienda_controlador@eliminarProducto');

Route::put('mensajeEliminado/{id}','AJAx\practica_tienda_controlador@mensajeEP');











Route::get('eliminarporbandera_datos/{id}','AJAx\practica_tienda_controlador@editar_datos');



Route::get('/', function () {

	return view('welcome');
});



//api
Route::get('ws/{id}','Auth\LoginController@obtieneApi');

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
	Route::get('/', function () {
        return view('/home');
    });

    Route::resource('administracion/categoria','proyecto\CategoriaController');
    Route::resource('administracion/equipo','proyecto\EquipoController');
   /// Route::resource('administracion/ingresos','proyecto\CategoriaController');
    //CONTABILIDAD
    Route::resource('contabilidad/finanzas','proyecto\FinanzasController');
    Route::resource('contabilidad/inscripcion','proyecto\InscripcionController');
    Route::resource('contabilidad/fondomedico','proyecto\FondoMedicoController');
    Route::resource('contabilidad/otros','proyecto\OtrosController');
   
    
 }); 


Route::get('/home', 'HomeController@index')->name('home');
