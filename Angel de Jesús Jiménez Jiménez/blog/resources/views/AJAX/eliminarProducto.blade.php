<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Eliminar producto</title>
</head>


<body>

    {!!Form::open(array('url'=>'mensajeEliminado/'.$prod->id,'method'=>'PUT'
    ,'autocomplete'=>'off'))!!}

<table>
        <tr>
          <td>

           {!!Form::label('PRODUCTO:')!!}
           {!!Form::input('nombre','text',$prod->nombre)!!}

         </td>
        </tr>
        <tr>
          <td> 
            <br>
           {!!Form::label('PRECIO:')!!}
           {!!Form::input('precio','text',$prod->precio_venta)!!}
           
          </td>
        </tr>
</table>

<br>
{!!Form::submit('Eliminar',['name'=>'eliminar','id'=>'eliminar','content'=>'<span>Eliminar</span>'])!!} 
{!!Form::close()!!}
    
</body>
</html>
