<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<title></title>
</head>
{
<body>
	 <div class="table-responsive">
	 	!!Form::open(array('url'=>'insertar_ferreteria','method'=>'POST','autocomplete'=>'off'))!!}

 {!!Form::submit('INSERTAR',['name'=>'enviar','id'=>'grabar','content'=>'<span>Registrar</span>'])!!} 
 {!!Form::close()!!}
<table class="table">
	
	<tr>
		<td>ID</td>
		<td>RFC</td>
		<td>CURP</td>
		<td>NUMERO CTRL</td>
		<td>MATERIA 1</td>
		<td>MATERIA 2</td>
		<td>MATERIA 3</td>
		<td>CALIF 1</td>
		<td>CALIF 2</td>
		<td>CALIF 3</td>
		<td>PROMEDIO</td>
		<td>Fecha alta</td>
		<td colspan="3" align="center">Accion</td>
	</tr>
	@foreach($usuario as $c)
	   <tr>
	   	<td>{{$c->id}}</td>
	   	<td>{{$c->frc}}</td>
	   	<td>{{$c->curp}}</td>
	   	<td>{{$c->numControl}}</td>
	   	<td>{{$c->materia1}}</td>
	   	<td>{{$c->materia2}}</td>
	   	<td>{{$c->materia3}}</td>
	   	<td>{{$c->cal1}}</td>
	   	<td>{{$c->cal2}}</td>
	   	<td>{{$c->cal3}}</td>
	   	<td>{{$c->promedio}}</td>
	   	<td>{{$c->fecha_alta}}</td>
	   	<td><a href="actualizar_tabla/{{$c->id}}">[Editar]</a></td>
	   	<td><a href="eliminar_tabla/{{$c->id}}">[Eliminar]</a></td>
	   	<td><a href="eliminar_bandera/{{$c->id}}">[Eliminar por Bandera]</a></td>
	   </tr>
	   @endforeach

</table>
</div>
</body>
</html>
