<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<title></title>
</head>
<body>
	 <div class="table-responsive">

<table class="table">
	<tr>
		<td>ID</td>
		<td>RAZON SOCIAL</td>
		<td>GIRO</td>
		<td>DOMICILIO FISCAL</td>
		<td>RFC</td>
		<td>ESTADO</td>
		<td>AÑO DE INGRESO</td>
		<td colspan="3" align="center">Accion</td>
	</tr>
	@foreach($usuario as $c)
	   <tr>
	   	<td>{{$c->id}}</td>
	   	<td>{{$c->razon_social}}</td>
	   	<td>{{$c->giro}}</td>
	   	<td>{{$c->domicilio}}</td>
	   	<td>{{$c->rfc}}</td>
	   	<td>{{$c->estado}}</td>
	   	<td>{{$c->año_ingreso}}</td>

	   	<td><a href="actualizar_ferreteria/{{$c->id}}">[Editar]</a></td>
	   	<td><a href="eliminar_ferreteria/{{$c->id}}">[Eliminar]</a></td>
	   	<td><a href="eliminar_bandera_ferreteria/{{$c->id}}">[Eliminar por Bandera]</a></td>
	   </tr>
	   @endforeach

</table>
</div>
</body>
</html>
