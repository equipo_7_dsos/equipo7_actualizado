@extends ('layouts.dashboard')

@section ('contenido')
<center>
    <h3>BIENVENIDO AL SISTEMA DE CONTABILIDAD</h3><br>
    <img src="https://libereurope.eu/wp-content/uploads/2017/06/person-placeholder-5.png" width="150" height="150">

    <h4>Usuario: {{ Auth::user()->name }}</h4>
    <h4>Correo: {{ Auth::user()->email }}</h4>
    
</center>

@endsection