<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Contabilidad Futbool</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
<link href="./bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>CF</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Contablilidad</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <!--CHECAR ESTA PARTE DEL CODIGO PARA LE LOGIN-->
                    @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesion') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                        <!--CHECAR ESTA PARTE DEL CODIGO PARA LE LOGIN-->
                </ul>
            </ul>
          </div>

        </nav>
      </header>
      <!-- Columna del lado izquierdo contiene el logo y la barra lateral -->
      <aside class="main-sidebar">
        <!-- Barra lateral: el estilo se puede encontrar en sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->    
          <!-- Menú de la barra lateral: el estilo se puede encontrar en sidebar.less-->
          <ul class="sidebar-menu">
            <li class="header"></li>
            <center><span>ADMINISTRACION</span></center>
            <li><a href="{{url('administracion/equipo')}}"><i class="fa fa-group"></i> <span>Equipos</span></a></li>
            <li><a href="{{url('administracion/categoria')}}"><i class="fa fa-futbol-o"></i> <span>Categorias</span></a></li>
            <li><a href="#"><i class="fa fa-flag"></i> <span>Torneo</span></a></li>
            <center><span>CONTABILIDAD</span></center>
           
            <li class="treeview"  >
                          <a href="#" >
                <i class="fa fa-plus-square"></i> 
                <span class="">Ingresos</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu" >
                <li><a href="{{url('contabilidad/finanzas')}}"><i class="fa fa-circle-o"></i>Fianza</a></li>
                <li><a href="{{url('contabilidad/inscripcion')}}"><i class="fa fa-circle-o"></i>Inscripcion</a></li>
                <li><a href="{{url('contabilidad/fondomedico')}}"><i class="fa fa-circle-o"></i>Fondo Medico</a></li>
                <li><a href="{{url('contabilidad/otros')}}"><i class="fa fa-circle-o"></i>Otros</a></li>
              </ul>
            </li>


            <li class="treeview">
              <a href="#">
                <i class="fa fa-minus-square"></i>
                <span>Egresos</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i>Devoluciones</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Pagos</a></li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-calculator"></i> <span>Corte de Caja</span></a></li>
            <li><a href="#"><i class="fa fa-bar-chart"></i> <span>Balance General</span></a></li>       
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Reporte</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i>Reportes</a></li>
              </ul>
            </li>
            <center><span>CONTROL</span></center>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> 
                <span>Acceso</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Usuarios</a></li>
              </ul>
            </li>
             <!--<li>
              <a href="#">
                <i class="fa fa-plus-square"></i> <span>Ayuda</span>
                <small class="label pull-right bg-red">PDF</small>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa fa-info-circle"></i> <span>Acerca De...</span>
                <small class="label pull-right bg-yellow">IT</small>
              </a>
            </li>-->           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>


       <!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        
        <!-- Main content -->
        <section class="content">
          
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Contabilidad Futbool</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                              <!--Contenido-->
                              @yield('contenido')
                              <!--Fin Contenido-->
                           </div>
                        </div>
                      </div>
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <!--Fin-Contenido-->

      <!--Footer del sistema-->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>2019<a href="#"> Club Deportivo Pochutla</a></strong>
        <br><a href="#"><i class="fa fa-facebook-official"></i> <span>Facebook - Club Deportivo Pochutla</span></a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#"><i class="fa fa-envelope-square"></i> <span>Correo - Club Deportivo Pochutla</span></a>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#"><i class="fa fa-phone-square"></i> <span>Tel - Club Deportivo Pochutla</span></a>
      </footer>

      
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('js/jQuery-2.1.4.min.js')}}"></script>
    @stack('scripts')
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('js/app.min.js')}}"></script>
    

  </body>
</html>