@extends ('proyecto/layouts.dashboard')
@section ('contenido')
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Listado de Categorías <a href="equipo/create"><button class="btn btn-success">Nuevo</button></a></h3>
		@include('proyecto/administracion.equipo.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Opciones</th>
				</thead>
               @foreach ($equipos as $equ)
				<tr>
					<td>{{ $equ->idequipo}}</td>
					<td>{{ $equ->nombre}}</td>
					<td>
						<a href="{{URL::action('proyecto\EquipoController@edit',$equ->idequipo)}}"><i class="fa fa-edit"></i></a>
                         <a href="" data-target="#modal-delete-{{$equ->idequipo}}" data-toggle="modal"><i class="fa fa-trash-o"></i></a>
					</td>
				</tr>
				@include('proyecto/administracion.equipo.modal')
				@endforeach
			</table>
		</div>
		{{$equipos->render()}}
	</div>
</div>

@endsection