@extends ('proyecto/layouts.dashboard')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Nueva Categoría</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::open(array('url'=>'administracion/categoria','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" name="nombre" class="form-control" placeholder="Nombre...">
            </div>
            <div class="form-group">
            	<label for="comentario">Comentario</label>
            	<input type="text" name="comentario" class="form-control" placeholder="Comentario...">
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
    		<div class="form-group">                            
    			<label>Genero</label>
    			<select name="genero" class="form-control" required id="genero">
                    <option value="">Seleccione una opción</option>
                    <option value="Varonil">Varonil</option>
                    <option value="Femenil">Femenil</option>
    			</select>
    		</div>
    	   </div>
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
            <div class="form-group">                            
                <label>Rango Edad</label>
                <select name="edad" class="form-control" required id="edad">
                    <option value="">Seleccione una opción</option>
                    <option value="Varonil">Varonil</option>
                    <option value="Femenil">Femenil</option>
                </select>
            </div>
           </div>
            <div class="form-group">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<a href="{{url('administracion/categoria')}}"><button class="btn btn-danger" type="button">Cancelar</button></a>
            </div>

			{!!Form::close()!!}		
            
		</div>
	</div>
@endsection