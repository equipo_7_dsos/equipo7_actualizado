<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

    {!!Form::label('ID: ') !!}
    <br>
    {!!form::text('id',$uno->id) !!}

    <br>
    {!!Form::label('RFC: ') !!}
    <br>
    {!!form::text('rfc',$uno->rfc) !!}
    <br>
    {!!Form::label('CURP: ') !!}
    <br>
    {!!form::text('curp',$uno->curp) !!}
    <br>
    {!!Form::label('NUMERO DE CONTROL: ') !!}
    <br>
    {!!form::number('num_control',$uno->num_ctrl) !!}
    <br>
    {!!Form::label('MATERIA 1')!!}
    <br>
   {!!form::select ('materia1',array('espanol'=>'Español','matematicas'=>'Matematicas','ingles'=>'Ingles'),$uno->materia1) !!}
   <br>
   {!!Form::label('MATERIA 2') !!}
   <br>
  {!!form::select ('materia2',array('espanol'=>'Español','matematicas'=>'Matematicas','ingles'=>'Ingles'),$uno->materia2) !!}
  <br>
  {!!Form::label('MATERIA 3') !!}
  <br>
   {!!form::select ('materia3',array('espanol'=>'Español','matematicas'=>'Matematicas','ingles'=>'Ingles'),$uno->materia3) !!}
    <br>
    {!!Form::label('CALIFICACION MATERIA 1: ') !!}
    <br>
    {!!form::number('calif_m1',$uno->calif_m1) !!}
    <br>
    {!!Form::label('CALIFICACION MATERIA 2: ') !!}
    <br>
    {!!form::number('calif_m2',$uno->calif_m2) !!}
    <br>
    {!!Form::label('CALIFICACION MATERIA 3: ') !!}
    <br>
    {!!form::number('calif_m3',$uno->calif_m3) !!}
    <br>
    <br>
    {!!Form::label('PROMEDIO: ') !!}
    <br>
    {!!form::number('promedio',$uno->promedio) !!}
    <br>
    {!!Form::label('FECHA DE ALTA: ') !!}
    <br>
    {!!form::date('fecha_alta',$uno->fecha_alta) !!}
  
  </body>
</html>
