<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class practica3_model extends Model
{

	protected $table= 'tienda';
	protected $primarykey='id';
	public $timestamps=false;
	protected $fillable=[
     'id','razon_social','rfc','nombre_d','direccion','tipo_empresa','telefono','fecha_ingreso'
	];

}