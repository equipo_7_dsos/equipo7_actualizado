<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class api_model extends Model
{

	protected $table= 'municipios';
	protected $primarykey='idLocal';
	public $timestamps=false;
	protected $fillable=[
     'idLocal','clave','claveLocalidad','nombreLocalidad','ambito'
	];

}