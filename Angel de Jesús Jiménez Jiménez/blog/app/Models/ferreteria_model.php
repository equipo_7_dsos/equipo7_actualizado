<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class ferreteria_model extends Model
{

	protected $table= 'ferreteria';
	protected $primarykey='id';
	public $timestamps=false;// para desabilitar la creacion de dos columnas de fecha al momento de hacer la consulta
	protected $fillable=[
     'id','razon_social','giro','domicilio','rfc','estado','año_ingreso','bandera',
	];

}