<?php 

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class practica4_model extends Model
{

	protected $table= 'alumnocalif';
	protected $primarykey='id';
	public $timestamps=false;// para desabilitar la creacion de dos columnas de fecha al momento de hacer la consulta
	protected $fillable=[
     'id','rfc','curp','numControl','materia1','materia2','materia3','cal1','cal2','cal3','promedio','fecha_alta','activo'
	];

}