<?php 

namespace App\Models\pw;
use Illuminate\Database\Eloquent\Model;

/**
 * 
 */
class ejemplo_model extends Model
{

	protected $table= 'practica';
	protected $primarykey='id';
	public $timestamps=false;
	protected $fillable=[
     'id','nombre'
	];

}