<?php 

namespace App\Models\proyecto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class Categoria extends Model
{
	protected $table= 'categoria';
	protected $primarykey='idcategoria';
	public $timestamps=false;
     
	 protected $fillable =[
     'nombre',
     'comentario',
     'genero',
     'edad'
    ];
}