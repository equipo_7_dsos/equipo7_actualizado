<?php 

namespace App\Models\proyecto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
/**
 * 
 */
class Equipo extends Model
{
	protected $table= 'equipo';
	protected $primarykey='idequipo';
	public $timestamps=false;
     
	 protected $fillable =[
     'idcategoria',
     'nombre',
     'logo',
     'procedencia',
     'fecha_inscripcion'
    ];
}