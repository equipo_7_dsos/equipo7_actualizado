<?php

namespace App\Http\Controllers\proyecto;

use Illuminate\Http\Request;
use App\Models\proyecto\Categoria;
use Illuminate\Support\Facades\Redirect;
use DB;
class OtrosController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $categorias=DB::table('categoria')
            ->where('nombre','LIKE','%'.$query.'%')
            ->orderBy('idcategoria','desc')
            ->paginate(7);
            return view('proyecto/administracion.categoria.index',["categorias"=>$categorias,"searchText"=>$query]);
        }
    }

    public function create()
    {
        return view("proyecto/administracion.categoria.create");
    }

    public function store (Request $request)
    {
        $categoria=new Categoria;
        $categoria->nombre=$request->get('nombre');
        $categoria->comentario=$request->get('comentario');
        $categoria->genero=$request->get('genero');
        $categoria->edad=$request->get('edad');
        $categoria->save();
        return Redirect::to('administracion/categoria');
    }

     public function show($id)
    {
        return view("proyecto/administracion.categoria.show",["categoria"=>Categoria::findOrFail($id)]);
    }

    public function edit($id)
    {
        return view("proyecto/administracion.categoria.edit",["categoria"=>Categoria::findOrFail($id)]);
    }

    public function update(Request $request, $id)
    {
        $categoria=Categoria::findOrFail($id);
        $categoria->nombre=$request->get('nombre');
        $categoria->comentario=$request->get('comentario');
        $categoria->genero=$request->get('genero');
        $categoria->edad=$request->get('edad');
        $categoria->update();
        return Redirect::to('administracion/categoria');
    }

    public function destroy($id)
    {
        $categoria=Categoria::findOrFail($id);
        $categoria->update();
        return Redirect::to('administracion/categoria');
    }
}
