<?php
namespace App\Http\Controllers;
namespace App\Http\Controllers\proyecto;

use Illuminate\Http\Request;
use App\Models\proyecto\Equipo;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use DB;
use Carbon\Carbon;
class EquipoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
    	if($request){
    		$query=trim($request->get('searchText'));
    		$equipos=DB::table('equipo as e')
    		->join('categoria as c','e.idcategoria','=','c.idcategoria')
    		->select('e.idequipo','e.nombre','e.logo','e.procedencia','e.fecha_inscripcion','c.nombre as categoria')
    		->where('e.nombre','LIKE','%'.$query.'%')
    		->orderBy('e.idequipo','desc')
    		->paginate(7);
    		return view('proyecto/administracion.equipo.index',["equipos"=>$equipos,"searchText"=>$query]);
    	}
    }

    public function create(){
    	$categorias=DB::table('categoria')->get();
    	return view("proyecto/administracion.equipo.create",["categorias"=>$categorias]);
    }
    public function store  (Request $request){
    	$equipo=new Equipo;
    	$equipo->idcategoria=$request->get('idcategoria');
    	$equipo->nombre=$request->get('nombre');
      $equipo->procedencia=$request->get('procedencia');

      $mytime = Carbon::now('America/Mexico_City');
      $equipo->fecha_inscripcion=$mytime->toDateTimeString();

      if(Input::hasFile('logo')){
      	$file=Input::file('logo');
      	$file->move(public_path().'/imagenes/logos/',$file->getClientOriginalName());
      	$equipo->logo=$file->getClientOriginalName();
      }

      $equipo->save();
      return Redirect::to('administracion/equipo');
    }

      public function edit($id)
    {
        return view("proyecto/administracion.equipo.edit",["equipo"=>Equipo::findOrFail($id)]);
    }

    public function update(Request $request, $id)
    {
        $categoria=Equipo::findOrFail($id);
        $categoria->nombre=$request->get('nombre');
        $categoria->comentario=$request->get('procedencia');
        $categoria->genero=$request->get('idcategoria');
        $categoria->update();
        return Redirect::to('administracion/equipo');
    }
   
    
}
