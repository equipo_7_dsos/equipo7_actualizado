<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\practica3_model; 
class practica3_controlador extends Controller
{
	
    
    public function insertar(Request $datos){
    	
        
    	$razon_social= $datos->input('razon_social');
    	$rfc= $datos->input('rfc');
    	$nombre_d= $datos->input('nombre_d');
    	$tipo_empresa=$datos->input('tipo_empresa');
    	$direccion=$datos->input('direccion');
    	$telefono=$datos->input('telefono');
        $fecha_ingreso=$datos->input('fecha_ingreso');
    	
          

    	practica3_model::create(['razon_social'=>$razon_social,'rfc'=>$rfc,'nombre_d'=>$nombre_d,'tipo_empresa'=>$tipo_empresa,'direccion'=>$direccion,'telefono'=>$telefono,'fecha_ingreso'=>$fecha_ingreso]);
        return redirect()->to('formulario');//para redireccionar a otra pagina

    }

     public function ver_formulario(){
      return view('vista_practica3');
     }
     
    //actualizar datos 
     public function editar_datos($id){
        $uno =practica3_model::where('id',$id)->take(1)->first();//muestra solamente un registro
        //$dos=practica3_model::find($id); hace lo mismo que $uno
      return view('vista_actualizardatos')->with('uno',$uno); 
     }

     public function ver_datos(){
         $uno=practica3_model::
         select('id','razon_social','rfc','nombre_d','direccion','tipo_empresa','telefono','fecha_ingreso')->take(1)->first();
         return view('vista_actualizardatos')->with('uno',$uno); 
    }

      public function actualizar_datos(Request $data,$id)
    {
      $editar = practica3_model::find($id);

      $editar->id = $data->id;
      $editar->razon_social = $data->razon_social;
      $editar->rfc = $data->rfc;
      $editar->nombre_d = $data->nombre_d;
      $editar->direccion = $data->direccion;
      $editar->tipo_empresa = $data->tipo_empresa;
      $editar->telefono = $data->telefono;
      $editar->fecha_ingreso = $data->fecha_ingreso;

      $editar->save();

      return redirect()->to('visualizar');



    }

}
