<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PW\practica5_modelo;
use App\Models\PW\modeloapi;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('guest')->except('logout');
    }
    public function getlogin()
    {
      return view('auth/login');
    }
   public function obtieneapi($nombre_cliente)
   {
  $respuesta = $this->peticion('GET',"http://dsos.com/api/obtener/{$nombre_cliente}");
$datos=json_decode($respuesta);
return response()->json($datos);


   }
    public function blacklist(Request $request)
    {
      $rfc = $request->rfc;
      return response()->json($consulta=practica5_modelo::select('rfc')
      ->where('rfc','=',$rfc)->take(1)->first());
    }
    public function practicaget(Request $request)
    {
      $nombre_cliente = $request->nombre_cliente;
      return response()->json($consulta=modeloapi::select('fecha_registro','sexo','fecha_nacimiento','id_cliente','nombre_cliente')
      ->where('nombre_cliente','=',$nombre_cliente)->take(1)->first());
    }
    public function practicainsertar(Request $request)
    {
      $fecha_registro = $request->fecha_registro;
      $sexo = $request->sexo;
      $fecha_nacimiento = $request->fecha_nacimiento;
      $id_cliente = $request->id_cliente;
      $nombre_cliente=$request->nombre_cliente;

      modeloapi::create(['fecha_registro'=>$fecha_registro,'sexo'=>$sexo,'fecha_nacimiento'=>$fecha_nacimiento,
                                'id_cliente'=>$id_cliente,'nombre_cliente'=>$nombre_cliente]);

      return response()->json(['mensaje'=>'Registrado correctamente',]);
    }
    public function practicaeliminar(Request $request)
    {
        $id_cliente = $request->id_cliente;
        modeloapi::select('fecha_registro','sexo','fecha_nacimiento','id_cliente','nombre_cliente')
        ->where('id_cliente','=',$id_cliente)
        ->delete();
        return response()->json(['mensaje'=>'Eliminado correctament',]);
    }
    public function practicaactualizar(Request $request)
    {
      $fecha_registro = $request->fecha_registro;
      $sexo = $request->sexo;
      $fecha_nacimiento = $request->fecha_nacimiento;
      $id_cliente = $request->id_cliente;
      $nombre_cliente=$request->nombre_cliente;

      modeloapi::select('fecha_registro','sexo','fecha_nacimiento','id_cliente','nombre_cliente')
      ->where('id_cliente','=',$id_cliente)
      ->update(['fecha_registro'=>$fecha_registro,'sexo'=>$sexo,'fecha_nacimiento'=>$fecha_nacimiento,
                                'id_cliente'=>$id_cliente,'nombre_cliente'=>$nombre_cliente]);

      return response()->json(['mensaje'=>'actualizado correctamente',]);
    }

    public function insertar_black(Request $request)
    {
      $razon_social = $request->razon_social;
      $giro = $request->giro;
      $domicilio_fiscal = $request->domicilio_fiscal;
      $rfc = $request->rfc;
      $estado=$request->estado;
      $ano_ingreso=$request->ano;
      $bandera=$request->bandera;

      practica5_modelo::create(['razon_social'=>$razon_social,'giro'=>$giro,'domicilio_fiscal'=>$domicilio_fiscal,
                                'rfc'=>$rfc,'estado'=>$estado,'ano_ingreso'=>$ano_ingreso,'bandera'=>$bandera]);

      return response()->json(['mensaje'=>'Registrado correctamente',]);
    }



    public function actualizar_black(Request $request)
    {
      $id=$request->id;
      $razon_social = $request->razon_social;
      $giro = $request->giro;
      $domicilio_fiscal = $request->domicilio_fiscal;
      $rfc = $request->rfc;
      $estado=$request->estado;
      $ano_ingreso=$request->ano;
      $bandera=$request->bandera;

      $editar=practica5_modelo::select(['id','razon_social','giro','domicilio_fiscal','rfc','estado','ano_ingreso','bandera'])
      ->where('id','=',$id)
      ->update(['razon_social'=>$razon_social,'giro'=>$giro,'domicilio_fiscal'=>$domicilio_fiscal,'rfc'=>$rfc
              ,'estado'=>$estado,'ano_ingreso'=>$ano_ingreso,'bandera'=>$bandera]);

      return response()->json(['mensaje'=>'Actualizado correctamente',]);
    }

    public function eliminar_black(Request $request)
    {
      $id=$request->id;
      $editar=practica5_modelo::select(['id','razon_social','giro','domicilio_fiscal','rfc','estado','ano_ingreso','bandera'])
      ->delete('id','=',$id);

      return response()->json(['mensaje'=>'Eliminado correctamente',]);
    }

    /*
    public fuction obtieneApi($id){
    $respuesta=$this->peticion('GET',"http://sigma.rpv/api/black/{id}");
    $datos =json_decode($respuesta);
    return response()->json($datos);
   }

   public function apiRegistro(){
      //
   }
   
   public function apiActualiza($dato){
    $respuesta = $this->peticion('PUT', "http://sigma.vpr/api/auth/actualizar/{$dato}",[
    'headers'=>[
      'Content-Type' =>'application/x-www-form-urlencoded',
       'X-Requested-With'=>'XMLHttpRequest'
               ],
    'form_params'=>[
           'serie'=>'',
           'av'=> ''
                   ]
        ]);
    $datos = json_decode($respuesta);
    return  response()->json($datos);

   }

   public function apiElimina($dato){
    $respuesta =$this->peticion('PUT',"http://sigma.vpr/api/auth/eliminar/{$dato}");
    $datos= json_decode($respuesta);
    return response()->json($datos);
   }
    public function inicio()
    {
      return view('layouts/inicio');
    }

*/
    public function authenticate()
    {
        $cc = $this->validate(request(),['usuario'=>'required|string','password' => 'required|string']);


        //return $credentials;

        if (Auth::attempt($cc)) {
            // Authentication passed...
           // return 'exito';
           // return redirect()->intended('dashboard');
           return redirect()->to('principal');
           //return view('Administrador/inicio');
        }
        else {
            ?>
                <script>
                            alert("Error: Usuario y/o Contrasenas incorrectos");
                            document.location.href = "/";
                </script>
           <?php
          }
    }

}


   



