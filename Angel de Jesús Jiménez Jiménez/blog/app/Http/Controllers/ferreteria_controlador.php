<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ferreteria_model; 
class ferreteria_controlador extends Controller
{
	
    
    

     public function ver_formulario(){
      return view('vista_insertar_ferreteria');
     }

   
    public function insertar(Request $datos){
      
      $id=$datos->input('id');
      $razon_social= $datos->input('razon_social');
      $giro= $datos->input('giro');
      $direccion=$datos->input('domicilio');
      $rfc= $datos->input('rfc');
      $estado= $datos->input('estado');
      $año_ingreso=$datos->input('año_ingreso');
       $bandera='1';
          

     ferreteria_model::create(['id'=>$id,'razon_social'=>$razon_social,'giro'=>$giro,'domicilio'=>$direccion,'rfc'=>$rfc,'estado'=>$estado,'año_ingreso'=>$año_ingreso,'bandera'=>$bandera]);
        return redirect()->to('tabla_ferreteria');//para redireccionar a otra pagina

    }
     
    //actualizar datos 
     public function editar_datos($id){
        $uno =ferreteria_model::where('id',$id)->take(1)->first();//muestra solamente un registro
        //$dos=practica3_model::find($id); hace lo mismo que $uno
      return view('vista_actualizar_ferreteria')->with('uno',$uno); 
     }

   
   public function ver_tabla(){
         $uno=ferreteria_model::where('bandera','1')->get();
         return view('vista_tabla_ferreteria')->with('usuario',$uno); 
    }


    public function actualizar_datos(Request $data,$id)
    {
      $editar = ferreteria_model::find($id);
       

      $editar->id = $data->id;
      $editar->razon_social= $data->razon_social;
      $editar->giro = $data->giro;
      $editar->domicilio = $data->domicilio;
      $editar->rfc = $data->rfc;
       $editar->estado = $data->estado;
      $editar->año_ingreso = $data->año_ingreso;
      $editar->save();

      return redirect()->to('tabla_ferreteria');



    }
     public function eliminar_datos_bandera(Request $data,$id)//se puede quitar el request
    {
      $editar = ferreteria_model::find($id);
       
      $editar->bandera='0';
      $editar->save();

      return redirect()->to('tabla_ferreteria');
    }

     public function eliminar_datos(Request $data,$id)
  {
    $editar = ferreteria_model::find($id);

    $editar->delete();
    return redirect()->to('tabla_ferreteria');

  }

}
