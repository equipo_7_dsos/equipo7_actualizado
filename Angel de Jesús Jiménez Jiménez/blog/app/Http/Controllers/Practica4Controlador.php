<?php
 namespace app\http\Controllers;
 use Illuminate\Http\Request;
 use App\Http\Controllers\Controller;
 use App\Models\practica4_modelo;

 /**
  *
  */
 class Practica4Controlador extends Controller
 {


   public function ver_formulario()
   {
     return view('vista_practica4_insertar');
   }

   public function ver_datos()
   {
   $uno = practica4_modelo::
    select('id','rfc','curp','numControl','materia1','materia2','materia3','cal1','cal2','cal3','promedio','fecha_alta')->take(1)->first();
   return view('vista_practica4_verdatos')->with('uno',$uno);
   }

   public function edit_datos($id)
   {
     $uno=practica4_Modelo:: where('id',$id)->take(1)->first();
     //$dos=Practica_Modelo::find($id);
     return view('vista_practica4_actualizar')->with('uno',$uno);
   }
   public function actualizar_datos(Request $data,$id)
   {
     $editar = practica4_modelo::find($id);

     $editar->id = $data->id;
     $editar->rfc = $data->rfc;
     $editar->curp = $data->curp;
     $editar->num_ctrl = $data->num_ctrl;
     $editar->materia1 = $data->materia1;
     $editar->materia2 = $data->materia2;
     $editar->materia3 = $data->materia3;
     $editar->calif_m1 = $data->calif_m1;
     $editar->calif_m2 = $data->calif_m2;
     $editar->calif_m3 = $data->calif_m3;
     $editar->promedio = $data->promedio;
     $editar->fecha_alta = $data->fecha_alta;
     $editar->save();

//     return redirect()->to('ver_datos');
return view('vista_practica4_verdatos')->with('uno',$editar);

   }

  public function insertar(Request $request)
  {
     $id = $request->input('id');
     $rfc = $request -> input('rfc');
     $curp = $request -> input('curp');
     $num_control = $request -> input('num_control');
     $materia1 = $request -> input('materia1');
     $materia2 = $request -> input('materia2');
     $materia3 = $request -> input('materia3');
     $calif_m1 = $request -> input('calif_m1');
     $calif_m2 = $request -> input('calif_m2');
     $calif_m3 = $request -> input('calif_m3');
     $promedio = $request -> input('promedio');
     $fecha_alta = $request -> input('fecha_alta');

     practica4_modelo::create(['id' => $id, 'rfc' => $rfc,'curp' => $curp
                              ,'num_ctrl' => $num_control, 'materia1' => $materia1, 'materia2' => $materia2
                              , 'materia3' => $materia3, 'calif_m1' => $calif_m1 , 'calif_m2' => $calif_m2,
                               'calif_m3' => $calif_m3, 'promedio' => $promedio,'fecha_alta' => $fecha_alta]);

     return redirect()-> to('insertar_practica4');
  }
  public function eliminar(Request $data,$id)
  {
    $eliminar = practica4_modelo::find($id);
                //practica4_modelo::all();
                $x=$activo;
  }

  public function edit_eliminar($id)
  {
    $uno=practica4_Modelo:: where('id',$id)->take(1)->first();
    //$dos=Practica_Modelo::find($id);
    return view('vista_practica4_eliminar')->with('uno',$uno);
  }
  public function eliminar_datos(Request $data,$id)
  {
    $editar = practica4_modelo::find($id);

    $editar->delete();

//     return redirect()->to('ver_datos');
return view('vista_practica4_verdatos')->with('uno',$editar);

  }

 }


 ?>
