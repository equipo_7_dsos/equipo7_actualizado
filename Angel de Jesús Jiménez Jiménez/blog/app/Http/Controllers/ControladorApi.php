<?php

namespace App\Http\Controllers\PW2;
use Illuminate\Http\Request;
use App\Models\PW\ModeloApi;
use App\Models\PW\Prueba;
use App\Http\Controllers\Controller;


class ControladorApi extends Controller
{

    public function blacklist(Request $request){
        $nom_estab = $request->nom_estab;
        return response()->json($consulta=ModeloApi::select('id','nom_estab','raz_social')->where('nom_estab',"=",$nom_estab)->get()); 
    }

    
    public function insertar(Request $request){
        $nombre = $request->nombre;
        $semestre = $request->semestre;
        //DEmas atributos
        prueba::create(['nombre'=>$nombre,'semestre'=>$semestre]); 

        return response()->json(['mensaje' => 'Registrado correctamente']); 
    }

    public function actualizar(Request $request){
        $nombre = $request->nombre;
        $semestre = $request->semestre;
        $id = $request->id;
        $editar = prueba::select('id','nombre','semestre')
        ->where('id','=',$id)
        ->update(['nombre'=>$nombre,'semestre'=>$semestre]);
        return response()->json(['mensaje' => 'Actualizado correctamenteee']); 
    }
    public function eliminar($id){
        $editar = prueba::select('id','nombre','semestre')
        ->where('id','=',$id)
        ->delete();
        return response()->json(['mensaje' => 'Eliminado correctamenteee']); 
    }
//¨¨¨¨¨¨¨¨¨****************************************************************************+
    public function agregar(Request $request){
        $id = $request->id;
        $nomb = $request->nom_estab;
        $raz = $request->raz_social;
        $cod = $request->codigo_act;
        $act = $request->nombre_act;
        
        ModeloApi::create(['id'=>$id,'nom_estab'=>$nomb,'raz_social'=>$raz,'codigo_act'=>$cod,
                'nombre_act'=>$act]); 

        return response()->json(['mensaje' => 'Registrado correctamente,id='.$id]); 
    }

    public function buscar($nombre_act){
        //Buscar por nombre_act
        //$nombre = $request->nombre_act;
        return response()->json($consulta=ModeloApi::select('id','nom_estab','raz_social','nombre_act')->where('nombre_act',"=",$nombre_act)->get()); 
    }
    
   public function edit(Request $request){
        $id = $request->id;
        $nomb = $request->nom_estab;
        $raz = $request->raz_social;
        $cod = $request->codigo_act;
        $act = $request->nombre_act;
        
        $editar = ModeloApi::select('id','nom_estab','raz_social')
        ->where('id','=',$id)
        ->update(['id'=>$id,'nom_estab'=>$nomb,'raz_social'=>$raz,'codigo_act'=>$cod,
                'nombre_act'=>$act]); 
        return response()->json(['mensaje' => 'Actualizado correctamente ID='.$id]); 
    }
    public function elimina(Request $request){
        $id = $request->id;
        $editar = ModeloApi::select('id','nom_estab','raz_social')
        ->where('id','=',$id)
        ->delete();
        return response()->json(['mensaje' => 'Eliminado correctamente ID='.$id]); 
    }

    ///*********************************************************************
    public  function obtieneApi($nombre_act){
        $res = $this->peticion('GET',"http://api.edu.mx/api/auth/buscar/{$nombre_act}");
        $datos = json_decode($res);
        return response()->json($datos);
    }
    public function apiRegistra(){
        $id = "2";
        $nomb = "prueba2";
        $raz = "prueba2";
        $act = 
        $cod = "22";
        $act ="Pruebaapi22";
        $respuesta = $this->peticion('POST',"https://apidsos1997.000webhostapp.com/api/auth/insertar",
            ['headers'=>['Content-Type'=>'application/x-www-form-urlencoded','X-Requested-With'=>'XMLHttpRequest'],
        'form_params'=>['id'=>$id,'nom_estab'=>$nomb,'raz_social'=>$raz,'codigo_act'=>$cod,
        'nombre_act'=>$act ]]);
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }
    public function apiActualiza($id){
        $nomb = "pruebaeditar";
        $raz = "prueba2editado";        
        $cod = "3";
        $act = "apiprueba";
        $respuesta = $this->peticion('PUT',"http://api.edu.mx/api/auth/editar/{$id}",
            ['headers'=>['Content-Type'=>'application/x-www-form-urlencoded',
            'X-Requested-With'=>'XMLHttpRequest'],
            'form_params' => ['nom_estab' => $nomb,'raz_social'=>$raz,
            'codigo_act'=> $cod,'nombre_act'=>$act]]
        );
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }

    public function apiElimina($id){
        $respuesta = $this->peticion('PUT',"http://api.edu.mx/api/auth/eliminar/{$id}");
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }
}
