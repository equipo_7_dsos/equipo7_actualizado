<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;


class AjaxController extends Controller
{
    public function listado_alumnos($genero)
    {
        $al = Alumnos::select('id','nombre_completo','sexo')
        ->where('sexo',$genero)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }
}
?>