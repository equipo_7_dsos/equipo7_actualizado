<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\practica4_model; 
class tabla_controlador extends Controller
{
	
    
    

     public function ver_formulario(){
      return view('vista_practica4');
     }

     
    //actualizar datos 
     public function editar_datos($id){
        $uno =practica4_model::where('id',$id)->take(1)->first();//muestra solamente un registro
        //$dos=practica3_model::find($id); hace lo mismo que $uno
      return view('vista_actualizar_practica4')->with('uno',$uno); 
     }

   
   public function ver_tabla(){
         $uno=practica4_model::where('activo','1')->get();
         return view('vista_tabla')->with('usuario',$uno); 
    }


    public function actualizar_datos(Request $data,$id)
    {
      $editar = practica4_model::find($id);
       

      $editar->id = $data->id;
      $editar->rfc = $data->rfc;
      $editar->curp = $data->curp;
      $editar->numControl = $data->numControl;
      $editar->materia1 = $data->materia1;
      $editar->materia2 = $data->materia2;
      $editar->materia3 = $data->materia3;
      $editar->cal1 = $data->cal1;
      $editar->cal2 = $data->cal2;
      $editar->cal3 = $data->cal3;
      $editar->promedio = $data->promedio;
      $editar->fecha_alta = $data->fecha_alta;

      $editar->save();

      return redirect()->to('tabla');



    }
     public function eliminar_datos_bandera(Request $data,$id)//se puede quitar el request
    {
      $editar = practica4_model::find($id);
       
      $editar->activo='0';
      $editar->save();

      return redirect()->to('tabla');
    }

     public function eliminar_datos(Request $data,$id)
  {
    $editar = practica4_model::find($id);

    $editar->delete();
    return redirect()->to('tabla');

  }

}
