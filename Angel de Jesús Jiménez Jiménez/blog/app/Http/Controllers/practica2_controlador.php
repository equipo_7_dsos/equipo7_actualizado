<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\practica2_model; 
class practica2_controlador extends Controller
{
	
	

	public function ver_datos(){
		 $practica=practica2_model::
		 select('id','nombre','apellidop','apellidom','edad','direccion','telefono')->take(1)->first();
		 return view('vista_practica2')->with('variable',$practica); 
	}
    
    public function insertar(Request $datos){
    	$id= $datos->input('id');
    	$nombre= $datos->input('nombre');
    	$apellidop= $datos->input('apellidop');
    	$apellidom= $datos->input('apellidom');
    	$edad=$datos->input('edad');
    	$direccion=$datos->input('direccion');
    	$telefono=$datos->input('telefono');
    	
    	practica2_model::create(['id'=>$id,'nombre'=>$nombre,'apellidop'=>$apellidop,'apellidom'=>$apellidom,'edad'=>$edad,'direccion'=>$direccion,'telefono'=>$telefono]);
        return redirect()->to('practica2');//para redireccionar a otra pagina

    }

     public function ver_formulario(){
      return view('insertar_datos');
     }

}
