<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::group(['prefix'=>'auth'],function(){
      Route::get('black','Auth\LoginController@blacklist');
      //practica
      Route::get('obtener','Auth\LoginController@practicaget');
      Route::post('insertar-practica','Auth\LoginController@practicainsertar');
      Route::put('actualizar','Auth\LoginController@practicaactualizar');
      Route::put('eliminar','Auth\LoginController@practicaeliminar');
      
      /*
      Route::post('insertar','Auth\LoginController@insertar_black');
      Route::put('actualizar','Auth\LoginController@actualizar_black');
      Route::put('eliminar','Auth\LoginController@eliminar_black');
      */
});
?>




