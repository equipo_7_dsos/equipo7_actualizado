-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: contfutbool
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `comentario` varchar(200) DEFAULT NULL,
  `genero` varchar(40) NOT NULL,
  `edad` varchar(40) NOT NULL,
  `estado` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (5,'Primera Fuerza','Liga primera fuerza.','Varonil','18 +','Activo'),(6,'Infantil 2006-2007','Liga infantil 2006-207','Varonil','2006-2007','Activo'),(7,'Infantil 2008 -2009','Liga infantil 2008 -2009.','Varonil','-','Activo'),(8,'Infantil 2010-2012 Pony','Liga infantil 2010-2012 pony.','Varonil','-','Activo'),(9,'SUB15 Varonil','Liga sub15 varonil.','Varonil','15','Activo'),(10,'SUB15 Femenil','Liga sub15 femenil.','Femenil','15','Activo'),(11,'Femenil Libre','Liga femenil libre.','Femenil','18+','Activo'),(12,'Intermedia','Liga intermedia.','Varonil','-','Activo'),(13,'Master','Liga master.','Varonil','-','Activo');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `egreso`
--

DROP TABLE IF EXISTS `egreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `egreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtorneo` int(11) NOT NULL,
  `concepto` varchar(150) DEFAULT NULL,
  `monto` decimal(11,2) DEFAULT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `usuario` varchar(45) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_torneo_egreso_idx` (`idtorneo`),
  CONSTRAINT `fk_torneo_egreso` FOREIGN KEY (`idtorneo`) REFERENCES `torneo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `egreso`
--

LOCK TABLES `egreso` WRITE;
/*!40000 ALTER TABLE `egreso` DISABLE KEYS */;
INSERT INTO `egreso` VALUES (4,8,'Alimentos y bebidas para bande de guerra.',960.00,'2019-06-10 17:05:36','Jonathan Toral Villalobos','Activo'),(5,8,'Sonido e iluminacion.',1600.00,'2019-06-10 17:05:47','Jonathan Toral Villalobos','Activo'),(6,8,'Gasolina podadora limpieza chapinguito.',150.00,'2019-06-10 17:05:56','Jonathan Toral Villalobos','Activo'),(7,8,'M.O. limpieza parcela.',200.00,'2019-06-10 17:06:06','Jonathan Toral Villalobos','Activo'),(8,8,'Gasolina y cuerda plastica podadora.',100.00,'2019-06-10 17:06:15','Jonathan Toral Villalobos','Activo'),(9,8,'Pagos de renta de sillas.',1000.00,'2019-06-10 17:08:08','Jonathan Toral Villalobos','Activo');
/*!40000 ALTER TABLE `egreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipo`
--

DROP TABLE IF EXISTS `equipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `logo` varchar(50) DEFAULT NULL,
  `procedencia` varchar(45) NOT NULL,
  `estado` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categoria_equipo_idx` (`idcategoria`),
  CONSTRAINT `fk_categoria_equipo` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (2,5,'La pasadita',NULL,'-','Activo'),(3,5,'Lomas del Carmen',NULL,'Lomas del Carmen','Activo'),(4,5,'Oxigeno Fuor',NULL,'Xoxocotlan','Activo'),(5,5,'Zapotengo',NULL,'Zapotengo','Activo'),(6,5,'Cefor Lafayette A',NULL,'-','Activo'),(7,13,'UDB',NULL,'-','Activo'),(8,13,'Zipolite',NULL,'Zipolite','Activo'),(9,13,'Barrio Chico',NULL,'Barrio Chico','Activo'),(10,13,'Dragones',NULL,'-','Activo'),(11,13,'Deportivo Costeño',NULL,'Puesto Escondido','Activo'),(12,6,'Hormigas \"A\"',NULL,'-','Activo'),(13,6,'Hormigas \"B\"',NULL,'-','Activo'),(14,6,'Tonameca Junior',NULL,'-','Activo'),(15,6,'Cefor Lafayette',NULL,'-','Activo'),(16,6,'Zipolite',NULL,'Zipolite','Activo'),(17,7,'Hormigas \"A\"',NULL,'-','Activo'),(18,7,'Hormigas \"B\"',NULL,'-','Activo'),(19,7,'Puerto Angel',NULL,'Puerto Angel','Activo'),(20,7,'Cefor Lafayette',NULL,'-','Activo'),(21,7,'Chacalapa',NULL,'Chacalapa','Activo'),(22,5,'Chubasco',NULL,'-','Activo');
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingreso`
--

DROP TABLE IF EXISTS `ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingreso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpago` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `idtorneo` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `monto` decimal(11,2) DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_detalle_pago_pago_idx` (`idpago`),
  KEY `fk_detalle_pago_equipo_idx` (`idequipo`),
  KEY `fk_ingreso_torneo_idx` (`idtorneo`),
  CONSTRAINT `fk_ingresos_equipo` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingresos_pago` FOREIGN KEY (`idpago`) REFERENCES `pago` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingresos_torneo` FOREIGN KEY (`idtorneo`) REFERENCES `torneo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingreso`
--

LOCK TABLES `ingreso` WRITE;
/*!40000 ALTER TABLE `ingreso` DISABLE KEYS */;
INSERT INTO `ingreso` VALUES (63,6,22,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(64,6,6,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(65,6,5,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(66,6,4,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(67,6,3,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(68,6,2,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(69,5,22,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(70,5,6,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(71,5,5,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(72,5,4,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(73,5,3,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(74,5,2,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Activo'),(75,5,3,8,'2019-06-10',500.00,'Jonathan Toral Villalobos','Inactivo'),(76,1,22,8,'2019-06-10',190.00,'Jonathan Toral Villalobos','Activo'),(77,1,6,8,'2019-06-10',190.00,'Jonathan Toral Villalobos','Activo'),(78,1,5,8,'2019-06-10',190.00,'Jonathan Toral Villalobos','Activo'),(79,1,4,8,'2019-06-10',190.00,'Jonathan Toral Villalobos','Activo'),(80,1,3,8,'2019-06-10',190.00,'Jonathan Toral Villalobos','Activo'),(81,1,2,8,'2019-06-10',190.00,'Jonathan Toral Villalobos','Activo');
/*!40000 ALTER TABLE `ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jornada`
--

DROP TABLE IF EXISTS `jornada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jornada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtorneo` int(11) NOT NULL,
  `idequipo` int(11) NOT NULL,
  `estado` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jornada_torneo_idx` (`idtorneo`),
  KEY `fk_jornada_equipo_idx` (`idequipo`),
  CONSTRAINT `fk_jornada_equipo` FOREIGN KEY (`idequipo`) REFERENCES `equipo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jornada_torneo` FOREIGN KEY (`idtorneo`) REFERENCES `torneo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jornada`
--

LOCK TABLES `jornada` WRITE;
/*!40000 ALTER TABLE `jornada` DISABLE KEYS */;
/*!40000 ALTER TABLE `jornada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago`
--

DROP TABLE IF EXISTS `pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` varchar(200) NOT NULL,
  `estado` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago`
--

LOCK TABLES `pago` WRITE;
/*!40000 ALTER TABLE `pago` DISABLE KEYS */;
INSERT INTO `pago` VALUES (1,'Arbitraje','Activo'),(2,'Fondo Medico','Activo'),(3,'Pitar Cancha','Inactivo'),(4,'Mantenimiento','Activo'),(5,'Fianza','Activo'),(6,'Inscripcion','Activo'),(7,'Enmicados','Activo'),(8,'Barda','Activo');
/*!40000 ALTER TABLE `pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneo`
--

DROP TABLE IF EXISTS `torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_torneo_categoria_idx` (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneo`
--

LOCK TABLES `torneo` WRITE;
/*!40000 ALTER TABLE `torneo` DISABLE KEYS */;
INSERT INTO `torneo` VALUES (2,11,'Clausura 2019 - 2020 Femenil Libre','Activo'),(5,11,'Apertura 2019 - 2020 Feminil Libre','Activo'),(6,5,'Apertura 2019 - 2020 Primera Fuerza','Activo'),(7,13,'Clausura 2019 - 2020 Master','Activo'),(8,5,'LIGA PRIMERA FUERZA CLAUSURA 2018.','Activo'),(9,13,'Apertura 2019 - 2020 Master','Activo');
/*!40000 ALTER TABLE `torneo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tipo_persona` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Jonathan Toral Villalobos','jona007jt@gmail.com',NULL,'$2y$10$iwNaYQquPDsfYMvGUU9XR.f7CYhSvX6CUWYcsKNcyMueTC40Mktxy','UfmdfcKWJ7m8goFWbGMhVYdmuAzfvM93Wo8vUXoyHZKuoiDOopV8foGhNjEv','2019-02-25 11:00:59','2019-02-25 11:00:59','Activo'),(2,'Diego Garcia Mijangos','diego02@gmail.com',NULL,'$2y$10$lrwlaaljs2ZenkkONOJiTuOXd9iXXi920JOllWafvJb.UsPDcpIv6','Wz9xOjNxPNpAqV3DCMW3Ar6b0kRmSNvgsvFe1LPruIjt6pnz23u0Ooq9q4xU','2019-02-25 13:17:09','2019-02-25 13:17:09','Activo'),(3,'Arnold Vasquez Hernandez','arnold02@gmail.com',NULL,'$2y$10$44.m0EGx1OgDRbBZr841z.2jTEfDz8jWwwLv3w7j9DjCcUMpId9EW','hZibUa0Ted3xGIAiinfLUJMbmC7NhUjuyubafiZbCvDtTJMIb3bJFL6P8WSz','2019-02-25 13:19:24','2019-02-25 13:19:24','Activo'),(4,'Marisol Vasquez Toral','marisol02@gmail.com',NULL,'$2y$10$B8MKTHomA5NBbS33NWAeFuVzsAuaggDo73afWFwLKfZ3AeBITXKWa','GmcIGVDlMBRj06eI8Xalfr825BK2Ej3TBvluHGm8lWY4nChzL9mnpVf6HwgY','2019-02-25 13:20:31','2019-02-25 13:20:31','Activo'),(5,'Juan Ruiz Lopez','juan02@gmail.com',NULL,'$2y$10$wd8WUMu6BUpaiIcdoHhv6.8/eJbwgtPAsgeuNqkO4AV52lb/fe0.y','8caE51q7iVYgztt5Zbi7zlmuU5AwrxgGtxzIKKKo6gJ7vB0FAqndlRtciYAS','2019-02-25 13:21:25','2019-02-25 13:21:25','Activo'),(7,'Angel Jimenez','angel@gmail.com',NULL,'$2y$10$qQXaM8ItmEfbzGSe8eo9i.oUHLE.XcNvZTvPWmYpCOTcRz0rRhujS',NULL,'2019-06-06 23:00:08','2019-06-10 23:44:01','Activo');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'contfutbool'
--

--
-- Dumping routines for database 'contfutbool'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-10 14:10:14
