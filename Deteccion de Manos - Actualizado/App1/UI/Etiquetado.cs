﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;

namespace App1.UI
{
    public partial class Etiquetado : Form
    {
        private Image<Bgr, int> img;            //Imagen a cargar

        private int max;                        //Difernecia maxima con el template
        private int min;                        //Diferencia minima con el template
        private int min2;                        //Diferencia minima con el template
        private int h;
        private int w;
        private int[,] binaria;                 //MAtriz binaria
        private int[,] binaria2;                 //MAtriz binaria
        private int[,] template;
        private int[,] diferencias;				//Diferncias entre el template
        //Lista de listas de equivalencias
        private List<List<int>> equivalencias = new List<List<int>>();

        public Etiquetado()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            equivalencias.Clear();
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
            {
                img = new Image<Bgr, int>(open.FileName);
            }
            binaria = aBinaria(img);
            binaria2 = (int[,])binaria.Clone();
            etiquetado();
            Console.WriteLine("Num obketos: {0}", equivalencias.Count);

            //dibujar();
            pictureBox1.Image = img.ToBitmap();

        }

        private void crearlista(int a, int b)
        {
            List<int> lis = new List<int>();
            lis.Add(a);
            lis.Add(b);
            equivalencias.Add(lis);
        }
        private int[,] aBinaria(Image<Bgr, int> ima)
        {
            //Minimos del rango de RGB
            int minrojo = 110;
            int minazul = 52;
            int minverde = 80;
            //Maximos de RGB
            int maxrojo = 188;
            int maxazul = 110;
            int maxverde = 143;
            int[,] bin = new int[ima.Rows, ima.Cols];
            for (int i = 0; i < ima.Rows; i++)
            {
                for (int j = 0; j < ima.Cols; j++)
                {
                    //Estableciendo el rango de colores
                    if (ima.Data[i, j, 2] <= maxrojo && ima.Data[i, j, 2] >= minrojo &&
                        ima.Data[i, j, 1] <= maxverde && ima.Data[i, j, 1] >= minverde &&
                        ima.Data[i, j, 0] <= maxazul && ima.Data[i, j, 0] >= minazul)
                    {
                        ima.Data[i, j, 2] = 255;
                        ima.Data[i, j, 1] = 255;
                        ima.Data[i, j, 0] = 255;
                        bin[i, j] = 1;
                    }
                    else
                    {
                        ima.Data[i, j, 2] = 0;
                        ima.Data[i, j, 1] = 0;
                        ima.Data[i, j, 0] = 0;
                        bin[i, j] = 0;
                    }

                }
            }
            return bin;
        }
        private int estaenlista(int v)
        {
            for (int i = 0; i < equivalencias.Count; i++)
            {
                if (equivalencias[i].Contains(v))
                    return i;
                /*for (int j = 0; j < equivalencias[i].Count; j++)
                {
                    if (v == equivalencias[i][j])
                    {
                        return i;
                    }
                }*/
            }
            return -1;
        }
        private void etiquetado()
        {
            //Recorrido de las matrices para el etiquetado
            int contador = 1;   //Contador para el etiquetado de los objetos
            for (int i = 1; i < img.Rows - 1; i++)
            {
                for (int j = 1; j < img.Cols - 1; j++)
                {
                    //Si la imagen binaria es diferente de cero
                    if (binaria[i, j] != 0)
                    {
                        //Si los vecinos no tienen etiqueta. Metodo de 8 conectado
                        if (binaria[i - 1, j - 1] == 0 && binaria[i - 1, j] == 0 &&
                            binaria[i, j - 1] == 0 && binaria[i - 1, j + 1] == 0)
                        {
                            binaria[i, j] = contador;
                            contador++;
                            //Establecindp una nueva region
                        }
                        else
                        {
                            if (binaria[i, j - 1] != 0)
                                binaria[i, j] = binaria[i, j - 1];
                            else if (binaria[i - 1, j] != 0)
                                binaria[i, j] = binaria[i - 1, j];
                            else if (binaria[i - 1, j - 1] != 0)
                                binaria[i, j] = binaria[i - 1, j - 1];
                            else binaria[i, j] = binaria[i - 1, j + 1];
                            //If para la creacion de listas de igualdad
                            if (binaria[i - 1, j] != 0 && binaria[i, j - 1] != 0
                                && binaria[i - 1, j] != binaria[i, j - 1])
                            {
                                //Creando lista de equivalencia
                                int x = binaria[i - 1, j];
                                int y = binaria[i, j - 1];
                                int auxx = estaenlista(x), auxy = estaenlista(y);
                                if (auxx == -1 && auxy == -1)
                                {
                                    crearlista(x, y);   //Se crea una nueva lista de equivalencia en caso de que 
                                    //los valores no esten asociados a una
                                }
                                else
                                {
                                    if (auxx == -1)
                                    {//If para verificar si x no existe a una lista y asociarlo a una
                                        //aux.push_back(y);
                                        equivalencias[auxy].Add(x); //Asociando x a la lista y
                                    }
                                    else equivalencias[auxx].Add(y);
                                    //En caso contrario se le asigna una equivalencia a y
                                }
                            }
                            else
                                if (binaria[i - 1, j + 1] != 0 && binaria[i, j - 1] != 0
                                    && binaria[i - 1, j + 1] != binaria[i, j - 1])
                                {
                                    int x = binaria[i - 1, j + 1];
                                    int y = binaria[i, j - 1];
                                    int auxx = estaenlista(x), auxy = estaenlista(y);
                                    if (auxx == -1 && auxy == -1)
                                    {
                                        crearlista(x, y);   //Se crea una nueva lista de equivalencia en caso de que 
                                        //los valores no esten asociados a una
                                    }
                                    else
                                    {
                                        if (auxx == -1)
                                        {//If para verificar si x no existe a una lista y asociarlo a una
                                            //aux.push_back(y);
                                            equivalencias[auxy].Add(x); //Asociando x a la lista y
                                        }
                                        else equivalencias[auxx].Add(y);
                                        //En caso contrario se le asigna una equivalencia a y
                                    }
                                }
                                else
                                    if (binaria[i - 1, j + 1] != 0 && binaria[i - 1, j - 1] != 0
                                        && binaria[i - 1, j + 1] != binaria[i - 1, j - 1])
                                    {
                                        int x = binaria[i - 1, j + 1];
                                        int y = binaria[i - 1, j - 1];
                                        int auxx = estaenlista(x), auxy = estaenlista(y);
                                        if (auxx == -1 && auxy == -1)
                                        {
                                            crearlista(x, y);   //Se crea una nueva lista de equivalencia en caso de que 
                                            //los valores no esten asociados a una
                                        }
                                        else
                                        {
                                            if (auxx == -1)
                                            {//If para verificar si x no existe a una lista y asociarlo a una
                                                //aux.push_back(y);
                                                equivalencias[auxy].Add(x); //Asociando x a la lista y
                                            }
                                            else equivalencias[auxx].Add(y);
                                            //En caso contrario se le asigna una equivalencia a y
                                        }
                                    }

                        }
                    }
                }
            }

            //Segundo recorrido para resolver quivalencias
            int listEq;
            for (int i = 1; i < img.Rows - 1; i++)
            {
                for (int j = 1; j < img.Cols - 1; j++)
                {
                    if (binaria[i, j] != 0)
                    {
                        listEq = estaenlista(binaria[i, j]);
                        if (listEq != -1)
                        {
                            binaria[i, j] = equivalencias[listEq][0];
                        }
                        else
                        {
                            List<int> aux = new List<int>();
                            aux.Add(binaria[i, j]);
                        }
                    }
                }
            }
        }
        private int[] areas()
        {
            int[] areas = new int[equivalencias.Count];
            areas.Initialize();
            for (int i = 0; i < img.Rows; i++)
            {
                for (int j = 0; j < img.Cols; j++)
                {
                    if (binaria[i, j] != 0)
                    {
                        int h;
                        for (h = 0; h < equivalencias.Count; h++)
                        {
                            if (binaria[i, j] == equivalencias[h][0])
                            {
                                areas[h] += 1;
                                break;
                            }
                        }
                    }
                }
            }
            return areas;
        }
        private int areaMayor()
        {
            int[] area = areas();
            int mayor = area.Max();
            for (int i = 0; i < area.Count(); i++)
            {
                if (area[i] == mayor)
                    return i;
            }
            return 0;
        }
        private int[] vertices()
        {
            //Obtencion del indice en la lista de objetos con la figura con mayor area
            int indice = areaMayor();
            int etiqueta = equivalencias[indice][0];
            //Recorriendo la figura para obtener los minimos y maximos en x y y
            int[] puntos = new int[4];
            puntos.Initialize();
            puntos[0] = img.Rows;
            puntos[1] = img.Cols;
            //i = x y = j
            //Primer recorrido para encontrar la esquina superior izq
            ///bool encontrado = false;
            for (int i = 1; i < img.Rows - 1; i++)
            {
                for (int j = 1; j < img.Cols - 1; j++)
                {
                    if (binaria[i, j] == etiqueta)
                    {
                        if (i < puntos[0])
                        {
                            puntos[0] = i;
                        }
                        if (j < puntos[1])
                            puntos[1] = j;
                        break;
                    }
                }
            }

            for (int i = img.Rows - 1; i > 1; i--)
            {
                for (int j = img.Cols - 1; j > 1; j--)
                {
                    if (binaria[i, j] == etiqueta)
                    {
                        if (i > puntos[2])
                        {
                            puntos[2] = i;
                        }
                        if (j > puntos[3])
                            puntos[3] = j;
                        break;
                    }
                }
            }

            return puntos;
        }

        private void dibujar()
        {
            int[] puntos = vertices();
            Console.WriteLine("Vertices X1: " + puntos[1] + " Y1: "
                            + puntos[0] + " \nX2: " + puntos[3] + " Y2: " + puntos[2]);
            Point p1 = new Point();
            p1.X = puntos[1];
            p1.Y = puntos[0];
            Point p2 = new Point();
            p2.X = puntos[3];
            p2.Y = puntos[2];
            MCvScalar color = new MCvScalar(0, 0, 255);
            //CvInvoke.cvRectangle(img,p1,p2,color,1,Emgu.CV.CvEnum.LINE_TYPE.FOUR_CONNECTED,4);
            //CvInvoke.Recta
            int h = p2.Y - p1.Y;
            int w = p2.X - p1.X;
            Console.WriteLine("Altura: " + h + " Ancho: " + w);
            Rectangle rec = new Rectangle(p1.X, p1.Y, w, h);
            Bgr col = new Bgr(70, 255, 255);
            img.Draw(rec, col, 2);
        }
        private void areasPequeñas()
        {
            int[] ares = areas();
            List<int> etis = new List<int>();
            for (int i = 0; i < equivalencias.Count; i++)
            {
                if (ares[i] <= 17)
                {
                    etis.Add(equivalencias[i][0]);
                }
            }
            for (int i = 0; i < etis.Count; i++)
            {
                for (int j = 0; j < equivalencias.Count; j++)
                {
                    if (equivalencias[j][0] == etis[i])
                    {
                        equivalencias.RemoveAt(j);
                        break;
                    }

                }
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void button4_Click(object sender, EventArgs e)
        {
            areasPequeñas();
            Console.WriteLine("Num obketos: {0}", equivalencias.Count);
            for (int i = 0; i < img.Rows; i++)
            {
                for (int j = 0; j < img.Cols; j++)
                {
                    if (binaria[i, j] != 0 && estaenlista(binaria[i, j]) == -1)
                    {
                        img.Data[i, j, 2] = 0;
                        img.Data[i, j, 1] = 0;
                        img.Data[i, j, 0] = 0;
                        binaria[i, j] = 0;

                    }

                }
            }
            pictureBox1.Image = null;
            pictureBox1.Image = img.ToBitmap();

        }
        private int diferencia_template(int x, int y)
        {
            x = x - h / 2;
            y = y - w / 2;
            int diferencia = 0;
            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w; j++)
                {
                    try
                    {
                        if (binaria2[i + x, j + y] != template[i, j])
                        {
                            diferencia++;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception: " + e.Message);
                    }

                }
            }
            return diferencia;
        }
        private Point template_matching()
        {
            min = template.Length;
            Console.WriteLine("TAmaño del areglo: " + min);
            max = 0;
            int dif;
            int[] puntos = new int[4];
            Point p = new Point();
            diferencias = new int[img.Rows, img.Cols];
            diferencias.Initialize();
            //Recorrinedo la matriz
            for (int i = 0; i < img.Rows - h; i++)
            {
                for (int j = 0; j < img.Cols - w; j++)
                {
                    if (binaria[i, j] != 0)
                    {
                        dif = diferencia_template(i, j);
                        diferencias[i, j] = dif;
                        if (dif < min)
                        {
                            min = dif;
                            puntos[0] = i;
                            puntos[1] = j;
                            p.X = j;
                            p.Y = i;
                        }
                        else if (dif > max)
                        {
                            max = dif;
                        }
                    }
                }
            }
            //int promedio = (min + max) / 2;
            Console.WriteLine("Minimo hasta ahora: " + min);
            Console.WriteLine("Maximo hasta ahora: " + max);

            /*for (int i = 0; i < img.Rows - h; i++)
            {
                for (int j = 0; j < img.Cols - w; j++)
                {
                    //Condicion para obtener el segundo con menor diferencias
                    if (diferencias[i, j] != 0 && diferencias[i, j] < max &&
                        diferencias[i, j] > min && diferencias[i, j] < min2)
                    {
                        min2 = diferencias[i, j];
                        puntos[2] = i;
                        puntos[3] = j;
                    }						
                }
            }*/
            return p;
        }
        private Point buscarSegundo(Point primero)
        {
            min2 = max;
            Point p = new Point();

            int[] puntos = new int[4];
            //int promedio = (min + max) / 2;
            Console.WriteLine("Minimo hasta ahora: " + min);
            Console.WriteLine("Maximo hasta ahora: " + max);
            for (int i = 0; i < img.Rows - h; i++)
            {
                for (int j = 0; j < img.Cols - w; j++)
                {
                    //Condicion para obtener el segundo con menor diferencias
                    if (diferencias[i, j] != 0 &&
                        ((i < primero.Y || i > primero.Y + h) ||
                        (j < primero.X || j > primero.X + w)) &&
                        diferencias[i, j] < min2)
                    {
                        min2 = diferencias[i, j];
                        p.X = j;
                        p.Y = i;
                    }
                }
            }
            Console.WriteLine("Segundo minimo: " + min2);
            Console.WriteLine("primero x: " + primero.X);
            Console.WriteLine("primero y: " + primero.Y);
            Console.WriteLine("segundo x: " + p.X);
            Console.WriteLine("segundo y: " + p.Y);
            return p;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //Cargando el template
            OpenFileDialog open = new OpenFileDialog();
            Image<Bgr, int> temp;
            if (open.ShowDialog() == DialogResult.OK)
            {
                temp = new Image<Bgr, int>(open.FileName);
                template = aBinaria(temp);
                h = temp.Rows;
                w = temp.Cols;
                pictureBox2.Image = temp.ToBitmap();
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            //int[] puntos = template_matching();
            Console.WriteLine("Diferencia min = " + min);
            Console.WriteLine("Diferencia min2 = " + min2);
            //Dibujando ventanas
            Point p1 = template_matching();
            //p1.X = puntos[0];
            p1.X = p1.X - w / 2;
            //p1.Y = puntos[1];
            p1.Y = p1.Y - h / 2;
            Point p2 = buscarSegundo(p1);
            //p2.X = puntos[2];
            //p2.Y = puntos[3];
            p2.X = p2.X - w / 2;
            p2.Y = p2.Y - h / 2;
            MCvScalar color = new MCvScalar(0, 0, 255);

            Console.WriteLine("x1: " + p1.X + " y1: " + p1.Y);
            Console.WriteLine("x2: " + p2.X + " y2: " + p2.Y);
            Console.WriteLine("Altura: " + h + " Ancho: " + w);
            Rectangle rec = new Rectangle(p1.X, p1.Y, w, h);
            Rectangle rec2 = new Rectangle(p2.X, p2.Y, w, h);
            Bgr col = new Bgr(70, 255, 255);
            img.Draw(rec, col, 2);
            img.Draw(rec2, col, 2);
            pictureBox1.Image = null;
            pictureBox1.Image = img.ToBitmap();
        }


    }
}
