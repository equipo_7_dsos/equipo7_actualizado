﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.UI;

namespace App1.UI
{

    public partial class Form1 : Form
    {

        // Variables Globales 

        Image<Bgr, int> imagenDeEntrada;
        Image<Bgr, int> imagenGris;
        Image<Bgr, int> imagenComponenteRojo;
        Image<Bgr, int> imagenComponenteAzul;
        Image<Bgr, int> imagenComponenteVerde;
        Image<Bgr, int> imagenNegativo;
        Image<Bgr, int> imagenBinarizada;
        

        //Image<Bgr, int> img;
        //Image<Gray, int> gri;
        Image<Bgr, byte> My_Imge, My_Imge_gris;



        int columnas;
        int filas;

        //int[] arrayByte;
        int[,] matrizGrises;
        //int[,] matrizLBPU;
        //String[] combinaciones;


        public Form1()
        {
            InitializeComponent();
            
        }
        // Dada una imagen de entrada, se extraen las caracteristicas de cada pixel 
        private void imagenEntrada() { 
            OpenFileDialog Openfile = new OpenFileDialog();
            if (Openfile.ShowDialog() == DialogResult.OK)
            {
                imagenDeEntrada = new Image<Bgr, int>(Openfile.FileName);
                
                //gri = new Image<Gray, int>(Openfile.FileName);

                //img = new Image<Bgr, int>(Openfile.FileName);
                //img = img.Resize(64, 128, Emgu.CV.CvEnum.INTER.CV_INTER_NN);

                My_Imge = new Image<Bgr, byte>(Openfile.FileName);
                My_Imge_gris = new Image<Bgr, byte>(Openfile.FileName);

                columnas = imagenDeEntrada.Cols;
                filas = imagenDeEntrada.Rows;
                matrizGrises = new int[filas, columnas];
                pictureBox1.Image = imagenDeEntrada.ToBitmap();
            }       
        }

        private void convertirAgris(){
            
            imagenGris = imagenDeEntrada;

            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    int promedio = (imagenDeEntrada.Data[i, j, 0] + imagenDeEntrada.Data[i, j, 1] + imagenDeEntrada.Data[i, j, 2]) / 3;
                        
                    imagenGris.Data[i, j, 0] = promedio;  //Blue
                    imagenGris.Data[i, j, 1] = promedio;  //Green
                    imagenGris.Data[i, j, 2] = promedio;  //Red   
                    matrizGrises[i, j] = promedio;
                    My_Imge_gris.Data[i,j,0] = (byte)promedio; 
                    My_Imge_gris.Data[i,j,1] = (byte)promedio;
                    My_Imge_gris.Data[i, j, 2] = (byte)promedio;
                }
            }
            pictureBox1.Image = My_Imge_gris.ToBitmap();
        }

        private void componenteRojo()
        {
            imagenComponenteRojo = imagenDeEntrada;

            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    int rojo = imagenDeEntrada.Data[i, j, 2];

                    imagenComponenteRojo.Data[i, j, 0] = 0;  //Blue
                    imagenComponenteRojo.Data[i, j, 1] = 0;  //Green
                    imagenComponenteRojo.Data[i, j, 2] = rojo;  //Red   
                }
            }
            pictureBox1.Image = imagenComponenteRojo.ToBitmap();
        }

        private void componenteVerde()
        {
            imagenComponenteVerde = imagenDeEntrada;

            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    int verde = imagenDeEntrada.Data[i, j, 1];

                    imagenComponenteVerde.Data[i, j, 0] = 0;  //Blue
                    imagenComponenteVerde.Data[i, j, 1] = verde;  //Green
                    imagenComponenteVerde.Data[i, j, 2] = 0;  //Red   
                }
            }
            pictureBox1.Image = imagenComponenteVerde.ToBitmap();
        }

        private void componenteAzul()
        {
            imagenComponenteAzul = imagenDeEntrada;

            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    int azul = imagenDeEntrada.Data[i, j, 0];

                    imagenComponenteAzul.Data[i, j, 0] = azul;  //Blue
                    imagenComponenteAzul.Data[i, j, 1] = 0;  //Green
                    imagenComponenteAzul.Data[i, j, 2] = 0;  //Red   
                }
            }
            pictureBox1.Image = imagenComponenteAzul.ToBitmap();
        }

        public void negativo()
        {
            imagenNegativo = imagenDeEntrada;

            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    imagenNegativo.Data[i, j, 0] = 255 - imagenNegativo.Data[i, j, 0];  //Blue
                    imagenNegativo.Data[i, j, 1] = 255 - imagenNegativo.Data[i, j, 1];  //Green
                    imagenNegativo.Data[i, j, 2] = 255 - imagenNegativo.Data[i, j, 2];  //Red   
                }
            }
            pictureBox1.Image = imagenNegativo.ToBitmap();
        }

        public void binarizacion()
        {
            imagenBinarizada = imagenDeEntrada;

            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    if (imagenBinarizada.Data[i, j, 2] >= 130 && imagenBinarizada.Data[i, j, 2] <= 210 &&
                        imagenBinarizada.Data[i, j, 1] >= 60 && imagenBinarizada.Data[i, j, 1] <= 168 &&
                        imagenBinarizada.Data[i, j, 0] >= 47 && imagenBinarizada.Data[i, j, 0] <= 130)
                    {

                        imagenBinarizada.Data[i, j, 0] = 255;
                        imagenBinarizada.Data[i, j, 1] = 255;
                        imagenBinarizada.Data[i, j, 2] = 255;
                    }
                    else
                    {
                        imagenBinarizada.Data[i, j, 0] = 0;
                        imagenBinarizada.Data[i, j, 1] = 0;
                        imagenBinarizada.Data[i, j, 2] = 0;
                    }

                }
            }
            pictureBox1.Image = imagenBinarizada.ToBitmap();
        }



        // ************************               EVENTOS            ******************************

        private void cromaticaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            //cromatica();    
        }

        private void escalaDeGrisesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            convertirAgris();
        }

        private void componenteRojoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            convertirAgris();
        }

        private void componenteVerdeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            componenteVerde();
        }

        private void componenteAzulToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            componenteAzul();
        }

        private void negativoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            negativo();
        }

        private void binarizacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
            binarizacion();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            imagenEntrada();
        }

        private void guardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap imagen = null;
            String nombre = imagenDeEntrada.ToString();
            imagen.Save("C://Users//Angel//Documents//Verano 2019 ITO//IA//Deteccion de Manos//" + nombre + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void lBPToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LPB lpb = new LPB();
            lpb.Show();
        }

        private void ventanaDeslizanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VentanaDeslizante vd = new VentanaDeslizante();
            vd.Show();

        }


        private void etiquetarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Etiquetado et = new Etiquetado();
            et.Show();
        }

        /**********************  VENTANA DESLIZANTE ***************************** 
         Incrementos en x y y - ejemplo x+10 y y+10
         Nosotros decidimos el incremento
         ventana de 64 x 128
         Obtener descriptores de la venta
         Los valores serán clasificados con el "clasificador que tenemos" 
         Se calcula el LBP 
         Calcular histogramas por bloques y hacer coincidir desplazamientos. Podriamos tomar 
          
         */

        
        //ventanaDeslizante();
    }         

}
