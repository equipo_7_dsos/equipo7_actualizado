﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using System.IO;

namespace App1.UI
{
    public partial class LPB : Form
    {

        int[,] gris;
        int[,] lbp2;
        Image<Bgr, int> img;
        Image<Gray, int> gri;
        String histogram;
        List<double> histogramita;
        String ruta;

        public LPB()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            // Leer la imagen 
            if (open.ShowDialog() == DialogResult.OK)
            {
                ruta = open.FileName;

                img = new Image<Bgr, int>(open.FileName);
                img = img.Resize(64, 128, Emgu.CV.CvEnum.INTER.CV_INTER_NN);
                gri = new Image<Gray, int>(open.FileName);
                gri = gri.Resize(64, 128, Emgu.CV.CvEnum.INTER.CV_INTER_NN);

                //Cargando a escala de grises
                gris = new int[img.Rows, img.Cols];
                for (int i = 0; i < img.Rows; i++)
                {
                    for (int j = 0; j < img.Cols; j++)
                    {
                        gris[i, j] = (img.Data[i, j, 2] + img.Data[i, j, 1] + img.Data[i, j, 0]) / 3;
                    }
                }
                pictureBox1.Image = gri.ToBitmap();
            }

          SaveFileDialog save = new SaveFileDialog();
            if(save.ShowDialog()==DialogResult.OK){
                ruta = save.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lbp();
            lbp2 = lbpuniforme();
            Image<Gray, int> ima = gri.Clone();
            Image<Gray, int> ima2 = gri.Clone();
            for (int i = 0; i < ima.Rows; i++)
            {
                for (int j = 0; j < ima.Cols; j++)
                {
                    ima[i, j] = new Gray(gris[i, j]);
                    ima2[i, j] = new Gray(lbp2[i, j]);
                }
            }
            histograma_bloques(4, 8);
            histograma_bloques_intermedios(4, 8);
            //Console.WriteLine(histogram);
            //escribir(histogram);
            pictureBox2.Image = ima.ToBitmap();
            pictureBox3.Image = ima2.ToBitmap();
        }

        private void lbp()
        {
            String binario = "";
            for (int i = 1; i < img.Rows - 1; i++)
            {
                for (int j = 1; j < img.Cols - 1; j++)
                {
                    binario = Nbinario(i, j);
                    gris[i, j] = Adecimal(binario);
                }
            }
        }
        private int decision(int centro, int vecino)
        {
            if (vecino >= centro)
                return 1;
            return 0;
        }
        //ULBP
        private int[,] lbpuniforme()
        {
            int[,] lbpu = (int[,])gris.Clone();
            for (int i = 0; i < img.Rows; i++)
            {
                for (int j = 0; j < img.Cols; j++)
                {
                    lbpu[i, j] = uniformidad(gris[i, j]);
                }
            }
            return lbpu;
        }
        private int uniformidad(int dec)
        {
            switch (dec)
            {
                //Valores con U = 0 
                case 255:
                    return 1;
                case 0:
                    return 2;
                //---U = 2
                case 127:
                    return 3;
                case 191:
                    return 4;
                case 223:
                    return 5;
                case 239:
                    return 6;
                case 247:
                    return 7;
                case 251:
                    return 8;
                case 253:
                    return 9;
                case 254:
                    return 10;

                //------ con 2 0
                case 63:
                    return 11;
                case 159:
                    return 12;
                case 207:
                    return 13;
                case 231:
                    return 14;
                case 243:
                    return 15;
                case 249:
                    return 16;
                case 252:
                    return 17;
                case 126:
                    return 18;
                //-- con 3 0
                case 31:
                    return 19;
                case 143:
                    return 20;
                case 199:
                    return 21;
                case 227:
                    return 22;
                case 241:
                    return 23;
                case 248:
                    return 24;
                case 124:
                    return 25;
                case 62:
                    return 26;

                //----- con 4 0 -------------------------
                case 15:
                    return 27;
                case 135:
                    return 28;
                case 195:
                    return 29;
                case 225:
                    return 30;
                case 240:
                    return 31;
                case 120:
                    return 32;
                case 60:
                    return 33;
                case 30:
                    return 34;

                ///__---- con 5 0 ------------------------------
                case 7:
                    return 35;
                case 131:
                    return 36;
                case 193:
                    return 37;
                case 224:
                    return 38;
                case 112:
                    return 39;
                case 56:
                    return 40;
                case 28:
                    return 41;
                case 14:
                    return 42;
                //---- con 6 0---------
                case 3:
                    return 43;
                case 129:
                    return 44;
                case 192:
                    return 45;
                case 96:
                    return 46;
                case 48:
                    return 47;
                case 24:
                    return 48;
                case 12:
                    return 49;
                case 6:
                    return 50;
                //-- con 7 0
                case 1:
                    return 51;
                case 128:
                    return 52;
                case 64:
                    return 53;
                case 32:
                    return 54;
                case 16:
                    return 55;
                case 8:
                    return 56;
                case 4:
                    return 57;
                case 2:
                    return 58;
                default:
                    return 0;
            }
        }
        private String Nbinario(int i, int j)
        {
            String binario = "";
            binario += "" + decision(gris[i, j], gris[i, j + 1]);
            binario += "" + decision(gris[i, j], gris[i - 1, j + 1]);
            binario += "" + decision(gris[i, j], gris[i - 1, j]);
            binario += "" + decision(gris[i, j], gris[i - 1, j - 1]);
            binario += "" + decision(gris[i, j], gris[i, j - 1]);
            binario += "" + decision(gris[i, j], gris[i + 1, j - 1]);
            binario += "" + decision(gris[i, j], gris[i + 1, j]);
            binario += "" + decision(gris[i, j], gris[i + 1, j + 1]);
            return binario;
        }
        private int Adecimal(String bin)
        {
            int dec = 0;
            for (int i = 0; i < 8; i++)
            {
                if (bin[i] == '1')
                    dec += (int)Math.Pow(2, 7 - i);
            }
            return dec;
        }
        //Histogramas
        private String histograma(int ii, int jj, int ifi, int jfi, int tam)
        {
            histogramita = new List<double>();
            double[] hist;
            hist = new double[59];
            hist.Initialize();
            String histo = "";
            for (int i = ii; i <= ifi; i++)
            {
                for (int j = jj; j <= jfi; j++)
                {
                    hist[lbp2[i, j]] = hist[lbp2[i, j]] + 1;
                }
            }
            histo = normalizar(hist, tam);
            //Console.WriteLine("Histograma:" + histo);
            return histo;
        }
        private String normalizar(double[] his, int max)
        {
            //Console.Out.WriteLine("Normalizar");
            String h = "";
            double suma = 0;
            double s = 0;
            for (int i = 0; i < his.Length; i++)
            {
                s = Math.Round(his[i] / max, 3);
                escribirEnArchivo(s);
                h += "" + s + ", ";
                histogramita.Add(s);
                suma += s;
            }
            return h;
        }
        private void histograma_bloques(int col, int filas)
        {
            // (4, 8)
            histogram = "";
            int segmentoC = img.Cols / col;     // = 16
            int segmentoR = img.Rows / filas;   // = 16
            int tam = segmentoC * segmentoR;    // = 32
            int paso = segmentoC / 2;
            //Sacando segmentos
            for (int i = 0; i < filas-1; i++)
            {
                for (int j = 0; j < col-1; j++)
                {
                    //Calcular el histograma para cada bloque
                    histogram += histograma((i * segmentoR) + paso , (j * segmentoC) + paso,
                                            ((i * segmentoR + paso) + segmentoR - 1), ((j * segmentoC+ paso) + segmentoC - 1), tam);

                }

            }
        }

        private void histograma_bloques_intermedios(int col, int filas)
        {
            // (4, 8)
            histogram = "";
            int segmentoC = img.Cols / col;     // = 16
            int segmentoR = img.Rows / filas;   // = 16
            int tam = segmentoC * segmentoR;    // = 256

            //Sacando segmentos
            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    //Calcular el histograma para cada bloque
                    histogram += histograma((i * segmentoR), (j * segmentoC),
                                            (i * segmentoR + segmentoR - 1), (j * segmentoC + segmentoC - 1), tam);
                }
            }
        }

        //Guardar en archivo
        private void escribir(String his){
            StreamWriter sw = new StreamWriter(@"C:\Users\Angel\Documents\Verano 2019 ITO\IA\Deteccion de Manos - Actualizado\archivo.txt", true);
            sw.WriteLine(his);
            sw.Close();
        }

        private void escribirEnArchivo(double s) {     
                StreamWriter sw = new StreamWriter(ruta, true);
                sw.WriteLine(s);
                sw.Close();
        }
    }
}
