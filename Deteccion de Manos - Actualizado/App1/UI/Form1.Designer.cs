﻿
namespace App1.UI
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operacionesBasicasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escalaDeGrisesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binarizacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negativoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cromaticaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componenteRojoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componenteVerdeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.componenteAzulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lBPToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.segmentacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etiquetarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventanaDeslizanteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.operacionesBasicasToolStripMenuItem,
            this.segmentacionToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(924, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirToolStripMenuItem,
            this.guardarToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.menuToolStripMenuItem.Text = "Archivo";
            this.menuToolStripMenuItem.Click += new System.EventHandler(this.menuToolStripMenuItem_Click);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.abrirToolStripMenuItem.Text = "Abrir ";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.abrirToolStripMenuItem_Click);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.guardarToolStripMenuItem.Text = "Guardar";
            this.guardarToolStripMenuItem.Click += new System.EventHandler(this.guardarToolStripMenuItem_Click);
            // 
            // operacionesBasicasToolStripMenuItem
            // 
            this.operacionesBasicasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.escalaDeGrisesToolStripMenuItem,
            this.binarizacionToolStripMenuItem,
            this.negativoToolStripMenuItem,
            this.cromaticaToolStripMenuItem,
            this.componenteRojoToolStripMenuItem,
            this.componenteVerdeToolStripMenuItem,
            this.componenteAzulToolStripMenuItem,
            this.lBPToolStripMenuItem1});
            this.operacionesBasicasToolStripMenuItem.Name = "operacionesBasicasToolStripMenuItem";
            this.operacionesBasicasToolStripMenuItem.Size = new System.Drawing.Size(126, 20);
            this.operacionesBasicasToolStripMenuItem.Text = "Operaciones Basicas";
            // 
            // escalaDeGrisesToolStripMenuItem
            // 
            this.escalaDeGrisesToolStripMenuItem.Name = "escalaDeGrisesToolStripMenuItem";
            this.escalaDeGrisesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.escalaDeGrisesToolStripMenuItem.Text = "Escala de grises";
            this.escalaDeGrisesToolStripMenuItem.Click += new System.EventHandler(this.escalaDeGrisesToolStripMenuItem_Click);
            // 
            // binarizacionToolStripMenuItem
            // 
            this.binarizacionToolStripMenuItem.Name = "binarizacionToolStripMenuItem";
            this.binarizacionToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.binarizacionToolStripMenuItem.Text = "Binarizacion";
            this.binarizacionToolStripMenuItem.Click += new System.EventHandler(this.binarizacionToolStripMenuItem_Click);
            // 
            // negativoToolStripMenuItem
            // 
            this.negativoToolStripMenuItem.Name = "negativoToolStripMenuItem";
            this.negativoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.negativoToolStripMenuItem.Text = "Negativo";
            this.negativoToolStripMenuItem.Click += new System.EventHandler(this.negativoToolStripMenuItem_Click);
            // 
            // cromaticaToolStripMenuItem
            // 
            this.cromaticaToolStripMenuItem.Name = "cromaticaToolStripMenuItem";
            this.cromaticaToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.cromaticaToolStripMenuItem.Text = "Cromatica";
            this.cromaticaToolStripMenuItem.Click += new System.EventHandler(this.cromaticaToolStripMenuItem_Click);
            // 
            // componenteRojoToolStripMenuItem
            // 
            this.componenteRojoToolStripMenuItem.Name = "componenteRojoToolStripMenuItem";
            this.componenteRojoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.componenteRojoToolStripMenuItem.Text = "Componente rojo";
            this.componenteRojoToolStripMenuItem.Click += new System.EventHandler(this.componenteRojoToolStripMenuItem_Click);
            // 
            // componenteVerdeToolStripMenuItem
            // 
            this.componenteVerdeToolStripMenuItem.Name = "componenteVerdeToolStripMenuItem";
            this.componenteVerdeToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.componenteVerdeToolStripMenuItem.Text = "Componente verde";
            this.componenteVerdeToolStripMenuItem.Click += new System.EventHandler(this.componenteVerdeToolStripMenuItem_Click);
            // 
            // componenteAzulToolStripMenuItem
            // 
            this.componenteAzulToolStripMenuItem.Name = "componenteAzulToolStripMenuItem";
            this.componenteAzulToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.componenteAzulToolStripMenuItem.Text = "Componente azul";
            this.componenteAzulToolStripMenuItem.Click += new System.EventHandler(this.componenteAzulToolStripMenuItem_Click);
            // 
            // lBPToolStripMenuItem1
            // 
            this.lBPToolStripMenuItem1.Name = "lBPToolStripMenuItem1";
            this.lBPToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.lBPToolStripMenuItem1.Text = "LBP";
            this.lBPToolStripMenuItem1.Click += new System.EventHandler(this.lBPToolStripMenuItem1_Click);
            // 
            // segmentacionToolStripMenuItem
            // 
            this.segmentacionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.etiquetarToolStripMenuItem,
            this.ventanaDeslizanteToolStripMenuItem});
            this.segmentacionToolStripMenuItem.Name = "segmentacionToolStripMenuItem";
            this.segmentacionToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.segmentacionToolStripMenuItem.Text = "Segmentacion";
            // 
            // etiquetarToolStripMenuItem
            // 
            this.etiquetarToolStripMenuItem.Name = "etiquetarToolStripMenuItem";
            this.etiquetarToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.etiquetarToolStripMenuItem.Text = "Etiquetado";
            this.etiquetarToolStripMenuItem.Click += new System.EventHandler(this.etiquetarToolStripMenuItem_Click);
            // 
            // ventanaDeslizanteToolStripMenuItem
            // 
            this.ventanaDeslizanteToolStripMenuItem.Name = "ventanaDeslizanteToolStripMenuItem";
            this.ventanaDeslizanteToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
            this.ventanaDeslizanteToolStripMenuItem.Text = "VentanaDeslizante";
            this.ventanaDeslizanteToolStripMenuItem.Click += new System.EventHandler(this.ventanaDeslizanteToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(34, 67);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(843, 470);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 629);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operacionesBasicasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem segmentacionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escalaDeGrisesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem binarizacionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negativoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem componenteRojoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem componenteVerdeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem componenteAzulToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem etiquetarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lBPToolStripMenuItem1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem cromaticaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventanaDeslizanteToolStripMenuItem;
    }
}

