﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;


namespace App1.UI
{
    

    public partial class VentanaDeslizante : Form
    {

        private Image<Bgr, int> img;            //Imagen a cargar
        private Image<Bgr, int> template;            //Imagen a cargar

                // *** Regresion Logistica ***
        List<List<double>> listaDescriptores;
        List<double> listaPesos;
        

        int numArchivos = 20;

        public VentanaDeslizante()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //equivalencias.Clear();
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
            {
                img = new Image<Bgr, int>(open.FileName);
                pictureBox1.Image = img.ToBitmap();
            }
            //binaria = aBinaria(img);
            //binaria2 = (int[,])binaria.Clone();
            //etiquetado();
            //Console.WriteLine("Num obketos: {0}", equivalencias.Count);
            //dibujar();

            // ***** CARGAR DESCRIPTORES ****** //

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //equivalencias.Clear();
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
            {
                template = new Image<Bgr, int>(open.FileName);
            }
            pictureBox2.Image = template.ToBitmap();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // ********** VENTANA DESLIZANTE *************** //

            // CARGAR DESCRIPTORES
            cargarDescriptores();

        }


        void cargarDescriptores()
        {
            generarPesos(); // generar pesos iniciales aleatorios
            generarDescriptores();  // generar los descriptores de las img de entrada

            Console.WriteLine(listaDescriptores.ElementAt(0).ElementAt(0));
            Console.WriteLine(listaDescriptores.ElementAt(0).ElementAt(1));
            Console.WriteLine(listaDescriptores.ElementAt(1).ElementAt(1));
           
            // Seleccionar archivo
            // aplicar formula 
            // aprendizaje 
            // 
        }

                    //   REGRESION LOGISTICA 
        private void regresionLogistica() {

            double suma = 0;
                for(int i=0; i< listaDescriptores.Count; i++){
                        suma += productoPunto( listaDescriptores.ElementAt(i), listaPesos); // producto punto 

                }
       
        }

        private void generarPesos(){
            
            listaPesos = new List<double>();
            var seed = Environment.TickCount;
            var random = new Random(seed);

            for (int i = 0; i < 3128 ; i++)
            {
                var value = random.NextDouble();
                listaPesos.Add(value);
            }
        }

        private void generarDescriptores() { 
            // Leer la carpeta completa con los 100 descriptores
            listaDescriptores = new List<List<double>>();
           for (int i = 1; i <=numArchivos; i++) {
                listaDescriptores.Insert(i-1,leerArchivo(i));    // leer archivo 1,2,3,4..n
            }
        }

        private List<double> leerArchivo(int numArchivo)
        {
            string[] lines = System.IO.File.ReadAllLines("C://Users//Angel//Documents//Verano 2019 ITO//IA//Deteccion de Manos - Actualizado//descriptor"+numArchivo+".txt");
            
            List<double> aux = new List<double>();

            foreach (string line in lines)
            {
                double valor = Convert.ToDouble(line);
                aux.Add(valor);
            }
            numArchivo++;
            return aux;
        }

        
        
        
        private double funcionLogistica(double productoPunto)
        {
            return 1;
        }

        private double productoPunto(List<double> a, List<double> b) {
            double y = 0.0;
            for (int i = 0; i < a.Count; i++)
            {
                y += a.ElementAt(i) * b.ElementAt(i);
            }
            return y;
            
        } 

        // VECTOR DE PESOS -> modelo 
        /*
         El vector de entrada será 6178 descriptores
         Producto punto del modelo por el descriptor 
         * si el resultado < 0 es fondo
         * si es mayor a cero va a ser el objeto 
         * si es igual a cero es nuestra decision si le asignamos persona o fondo 
         
         * Funcion logistica(y) = 1 / 1 + e ^ -y 
         * x = new int [6198]
         * 
         * w -> aprendizaje automatico 
         * 
         * x1 * w1 + x2 * w2 + xn * wn 
         * pasa directo a la funcion logistica 
         
         * 
         * vector de resultados en tupla por cuadrito , 1
         *  Valores aleatorios entre 0 y 1 
         *  w0 (k) = w0(k-1) - taza de aprendizaje *  1/M (desde j=0 hasta j=M hacer sumatoria (hw(x[j]) - Y[j] ) x0[j] 
         *  
         * 
         * 
         W es un unico vertor, que lo usaré para encontrar valores en base a muestras. 
         * 
         
         J(x) = -1/M Sumatoria hasta y (y^j)
         
          
          
         */
        }




    }

